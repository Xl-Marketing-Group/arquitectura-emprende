<?php
// styles of the child theme
function uni_bauhaus_theme_child_styles() {

    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.5.0' );

    wp_register_style( 'ball-clip-rotate-style', get_template_directory_uri() . '/css/ball-clip-rotate.css', '0.1.0' );
    wp_enqueue_style( 'ball-clip-rotate-style');

    wp_register_style( 'bxslider-style', get_template_directory_uri() . '/css/bxslider.css', '4.2.3' );
    wp_enqueue_style( 'bxslider-style');

    wp_register_style( 'fancybox-style', get_template_directory_uri() . '/css/fancybox.css', '2.1.5' );
    wp_enqueue_style( 'fancybox-style');

    wp_register_style( 'jscrollpane-style', get_template_directory_uri() . '/css/jscrollpane.css', '2.1.5' );
    wp_enqueue_style( 'jscrollpane-style');

    wp_register_style( 'unitheme-styles', get_template_directory_uri() . '/style.css', array('ball-clip-rotate-style', 'bxslider-style',
    'fancybox-style', 'jscrollpane-style'), '1.3.4', 'all' );
    wp_enqueue_style( 'unitheme-styles' );

    wp_register_style( 'unitheme-adaptive', get_template_directory_uri() . '/css/adaptive.css', array('unitheme-styles'), '1.3.4', 'screen' );
    wp_enqueue_style( 'unitheme-adaptive' );

    wp_register_style( 'unichild-styles', get_stylesheet_directory_uri() . '/style.css', array('unitheme-adaptive'), '1.3.4', 'screen' );
    wp_enqueue_style( 'unichild-styles' );

}
add_action( 'wp_enqueue_scripts', 'uni_bauhaus_theme_child_styles' );

// after setup of the child theme
add_action( 'after_setup_theme', 'uni_bauhaus_theme_child_setup' );
function uni_bauhaus_theme_child_setup() {

    // Enable featured image
    add_theme_support( 'post-thumbnails');

    // Add default posts and comments RSS feed links to head
    add_theme_support( 'automatic-feed-links' );

    // translation files for the child theme
    load_child_theme_textdomain( 'bauhaus-child', get_stylesheet_directory() . '/languages' );
}

// add the new role in the child theme
add_action('after_switch_theme', 'uni_bauhaus_theme_child_activation_func', 10);
function uni_bauhaus_theme_child_activation_func() {
    add_role( 'architector', esc_html__('Architector', 'bauhaus'), array('read' => true) );
	$instructor = get_role('architector');
	$instructor->add_cap('read');
    $instructor->add_cap('edit_published_posts');
    $instructor->add_cap('upload_files');
    $instructor->add_cap('publish_posts');
    $instructor->add_cap('delete_published_posts');
    $instructor->add_cap('edit_posts');
    $instructor->add_cap('delete_posts');
    update_option('posts_per_page', 9);
    flush_rewrite_rules();
}

// remove the new role on theme deactivation
add_action('switch_theme', 'uni_bauhaus_theme_child_deactivation_func');
function uni_bauhaus_theme_child_deactivation_func() {
    remove_role( 'architector' );
}

?>