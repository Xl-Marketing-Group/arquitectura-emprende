jQuery( document ).ready( function( $ ) {
    'use strict';

    var $page_template = $('#page_template'),
        $about_metabox = $('#about_meta_box'),
        $contact_meta_box = $('#contact_meta_box'),
        $servicepage_metabox = $('#servicepage_meta_box'),
        $portfoliotwo_metabox = $('#portfoliotwo_meta_box'),
        $portfoliothree_metabox = $('#portfoliothree_meta_box');

    $about_metabox.hide();

    $page_template.on("change", function() {

        switch ( $(this).val() ) {

            case 'templ-about.php' :
            $portfoliotwo_metabox.hide();
            $portfoliothree_metabox.hide();
            $about_metabox.show();
            $servicepage_metabox.hide();
            $contact_meta_box.hide();
            break;

            case 'templ-service.php' :
            $portfoliotwo_metabox.hide();
            $portfoliothree_metabox.hide();
            $servicepage_metabox.show();
            $about_metabox.hide();
            $contact_meta_box.hide();
            break;

            case 'templ-contact.php' :
            $portfoliotwo_metabox.hide();
            $portfoliothree_metabox.hide();
            $contact_meta_box.show();
            $servicepage_metabox.hide();
            $about_metabox.hide();
            break;

            case 'templ-portfolio-two.php' :
            $portfoliotwo_metabox.show();
            $portfoliothree_metabox.hide();
            $contact_meta_box.hide();
            $servicepage_metabox.hide();
            $about_metabox.hide();
            break;

            case 'templ-portfolio-five.php' :
            $portfoliotwo_metabox.show();
            $portfoliothree_metabox.hide();
            $contact_meta_box.hide();
            $servicepage_metabox.hide();
            $about_metabox.hide();
            break;

            case 'templ-portfolio-three.php' :
            $portfoliotwo_metabox.hide();
            $portfoliothree_metabox.show();
            $contact_meta_box.hide();
            $servicepage_metabox.hide();
            $about_metabox.hide();
            break;

            default :
            $portfoliotwo_metabox.hide();
            $portfoliothree_metabox.hide();
            $about_metabox.hide();
            $servicepage_metabox.hide();
            $contact_meta_box.hide();
            break;

        }

    }).change();

});