jQuery( document ).ready( function( $ ) {
    'use strict';
	//
	/*wp.customize( 'uni_home_slider_enable', function( value ) {
		value.bind( function( to ) {
		    if ( to === 'on' ) {
			    $( '#homeSlider' ).show();
            } else {
                $( '#homeSlider' ).hide();
            }
		} );
	} );*/

    wp.customize( 'option_tree[uni_address]', function( value ) {
		value.bind( function( to ) {
		    $( '#js-uni-address' ).html(to);
		} );
	} );
    wp.customize( 'option_tree[uni_phone]', function( value ) {
		value.bind( function( to ) {
		    $( '#js-uni-phone' ).html(to);
		} );
	} );
    wp.customize( 'option_tree[uni_email]', function( value ) {
		value.bind( function( to ) {
		    $( '#js-uni-email' ).html(to);
		} );
	} );
    wp.customize( 'option_tree[uni_home_contact_send_link_enable]', function( value ) {
		value.bind( function( to ) {
		    if ( to === 'on' ) {
			    $( '#js-uni-email-send-link' ).show();
            } else {
                $( '#js-uni-email-send-link' ).hide();
            }
		} );
	} );
    wp.customize( 'option_tree[uni_home_contact_send_link_text]', function( value ) {
		value.bind( function( to ) {
		    $( '#js-uni-email-send-link' ).text(to);
		} );
	} );
    wp.customize( 'option_tree[uni_home_contact_send_link_uri]', function( value ) {
		value.bind( function( to ) {
            $( '#js-uni-email-send-link' ).attr('href', to);
		} );
	} );

});