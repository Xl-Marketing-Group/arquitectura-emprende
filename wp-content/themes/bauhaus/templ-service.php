<?php
/*
*  Template Name: Services Page
*/
get_header();
$aUniAllowedHtmlWoA = uni_bauhaus_theme_allowed_html_wo_a();
$aUniAllowedHtmlWithA = uni_bauhaus_theme_allowed_html_with_a();
?>

	<section class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post();
        $aCustomData = get_post_custom( $post->ID );

        if ( !empty($aCustomData['uni_page_header_enable'][0]) ) {
            $sPageHeaderEnable = $aCustomData['uni_page_header_enable'][0];
        } else {
            $sPageHeaderEnable = 'off';
        }
        if ( $sPageHeaderEnable != 'off' ) {

            $iPostThumb = get_post_thumbnail_id( $post->ID );
            if ( !empty($iPostThumb) ) $aThumb = wp_get_attachment_image_src( $iPostThumb, 'full' );
            if ( !empty($aThumb) ) {
                $sBackgroundImageUrl = $aThumb[0];
            } else {
                $sBackgroundImageUrl = get_template_directory_uri() . '/images/placeholders/bg-about.png';
            }
        ?>
		<div class="screen1" style="background-image: url(<?php echo esc_url( $sBackgroundImageUrl ); ?>);">
			<div class="wrapper">
				<h2><?php if ( !empty($aCustomData['uni_service_title'][0]) ) { echo wp_kses( $aCustomData['uni_service_title'][0], $aUniAllowedHtmlWoA ); } else { esc_html_e('best service', 'bauhaus'); } ?></h2>
				<p><?php if ( !empty($aCustomData['uni_service_sub_title'][0]) ) { echo wp_kses( $aCustomData['uni_service_sub_title'][0], $aUniAllowedHtmlWoA ); } else { esc_html_e('It starts with an impossible idea that meets focused creativity and converts into great result', 'bauhaus'); }  ?></p>
			</div>
			<i class="arrowDown"></i>
		</div>

        <?php } ?>

		<div class="ourService">
			<div class="wrapper">

                <?php
                if ( !empty($aCustomData['uni_page_title_enable'][0]) ) {
                    $sPageTitleEnable = $aCustomData['uni_page_title_enable'][0];
                } else {
                    $sPageTitleEnable = 'off';
                }
                if ( $sPageTitleEnable != 'off' ) {
                ?>
				<h1 class="blockTitle"><?php the_title() ?></h1>
                <?php } ?>
                <?php if ( !empty($aCustomData['uni_sub_title_h1'][0]) ) { ?>
				<p class="ourServiceBlockDesc"><?php echo wp_kses( $aCustomData['uni_sub_title_h1'][0], $aUniAllowedHtmlWithA ); ?></p>
                <?php } ?>

            	<?php
                $sNumberOfServices = ( !empty($aCustomData['uni_number_of_services'][0]) ) ? $aCustomData['uni_number_of_services'][0] : 3;
            	$aPricesArgs = array(
            	    'post_type' => 'uni_price',
                    'posts_per_page' => $sNumberOfServices,
            	    'orderby' => 'menu_order',
                    'order' => 'asc'
            	);
            	$oPrices = new WP_Query( $aPricesArgs );
            	if ( $oPrices->have_posts() ) :
                    if ( $sNumberOfServices == 3 ) {
                    $i = 1;
                ?>
				<div class="serviceWrap clear">
                <?php
                    } else if ( $sNumberOfServices == 4 ) {
                    $i = 11;
                ?>
				<div class="serviceWrap2 clear">
                <?php
                    }
            	while ( $oPrices->have_posts() ) : $oPrices->the_post();
                $aPostCustom = get_post_custom( get_the_ID() );
                ?>
					<div class="serviceItem">
						<div class="serviceHead">
							<img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/serviceImg'.esc_attr($i).'.png'; ?>" alt="<?php the_title_attribute() ?>">
							<h4><span><?php the_title() ?></span></h4>
						</div>
						<div class="servicePrice">
							<p><em><?php echo esc_html( $aPostCustom['uni_currency'][0] ) ?></em><strong><?php echo esc_html( $aPostCustom['uni_price'][0] ) ?></strong></p>
							<p><?php echo esc_html( $aPostCustom['uni_type'][0] ) ?></p>
						</div>
						<div class="serviceDesc">
							<?php the_content() ?>
                            <?php if ( !isset( $aPostCustom['uni_service_display_order_btn'][0] ) || ( isset( $aPostCustom['uni_service_display_order_btn'][0] ) && $aPostCustom['uni_service_display_order_btn'][0] !== 'off' ) ) { ?>
							<a href="#orderServiceForm" class="orderServiceItem" data-title="<?php the_title_attribute() ?>" data-price="<?php echo esc_attr( $aPostCustom['uni_price'][0] ) ?>"><?php echo ( isset($aPostCustom['uni_service_order_btn_label'][0]) ) ? esc_html( $aPostCustom['uni_service_order_btn_label'][0] ) : esc_html__('Order', 'bauhaus') ?></a>
                            <?php } ?>
						</div>
					</div>
            	<?php
                $i++;
            	endwhile;
                ?>
				</div>
                <?php
                endif;
            	wp_reset_postdata();
                ?>
			</div>
		</div>

        <?php endwhile; endif;
        wp_reset_postdata(); ?>

		<div class="ourServiceDesc">
	<?php
	$aServicesArgs = array(
	    'post_type' => 'uni_service',
        'posts_per_page' => -1,
	    'orderby' => 'menu_order',
        'order' => 'asc',
        'meta_query' => array(
		    array(
			    'key'       => 'uni_service_display_services',
                'value'     => 'on',
			    'compare'   => '='
		    )
	    )
	);
	$oServices = new WP_Query( $aServicesArgs );
	if ( $oServices->have_posts() ) :
    $i = 1;
	while ( $oServices->have_posts() ) : $oServices->the_post();
        $aPostCustom = get_post_custom( $post->ID );
    ?>
			<div class="serviceDescItem<?php if ($i % 2 == 0) { echo ' rightOrientation'; } else { echo ' leftOrientation'; } ?>">
				<div class="wrapper clear">
					<div class="serviceDescThumb">
                    <?php
                    if ( !empty($aPostCustom['uni_service_services_image'][0]) ) {
                        $aPageGalleryIds = explode(',', $aPostCustom['uni_service_services_image'][0]);
                        if ( !empty($aPageGalleryIds) ) {
                    ?>
                            <?php echo wp_get_attachment_image( $aPageGalleryIds[0], 'unithumb-service' ); ?>
                        <?php } else { ?>
                            <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-service.png' ?>" alt="<?php the_title_attribute() ?>" width="545" height="300">
                        <?php } ?>
                    <?php } else { ?>
    					<img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-service.png' ?>" alt="<?php the_title_attribute() ?>" width="545" height="300">
                    <?php } ?>
					</div>
					<div class="serviceDescText">
						<div class="serviceDescTextWrap">
							<h4><?php the_title() ?></h4>
							<?php the_content() ?>
						</div>
					</div>
				</div>
			</div>
	<?php
    $i++;
	endwhile; endif;
	wp_reset_postdata();
    ?>
		</div>
	</section>

<?php get_footer(); ?>