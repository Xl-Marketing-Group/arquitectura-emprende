<?php
/*
*  Template Name: Portfolio Three
*/
get_header(); ?>

	<section class="container">

    <?php if (have_posts()) : while (have_posts()) : the_post();
        $aCustomData = get_post_custom( $post->ID );
    ?>

        <?php if ( isset($aCustomData['uni_portfoliothree_text_position'][0]) && $aCustomData['uni_portfoliothree_text_position'][0] == 'top' ) { ?>
        <div class="portfolioContentWrap">
            <div class="wrapper">
                <div class="singlePost">
                    <?php the_content() ?>
                </div>  
            </div>
        </div>
        <?php } ?>

		<div class="portfolio-v3 clear">
        <?php
        $aProjectsChosen = ( !empty($aCustomData['uni_portfoliothree_projects'][0]) ) ? maybe_unserialize($aCustomData['uni_portfoliothree_projects'][0]) : array();
        if ( !empty($aProjectsChosen) ) {
            $aProjectArgs = array(
                'post_type' => 'uni_project',
                'posts_per_page' => -1,
                'post__in' => $aProjectsChosen,
                'ignore_sticky_posts' => true,
	            'orderby' => 'menu_order',
                'order' => 'asc'
            );
        } else {
            $aProjectArgs = array(
                'post_type' => 'uni_project',
                'posts_per_page' => -1,
	            'orderby' => 'menu_order',
                'order' => 'asc'
            );
        }

        $oProjectQuery = new wp_query( $aProjectArgs );
        if ( $oProjectQuery->have_posts() ) :
        while ( $oProjectQuery->have_posts() ) : $oProjectQuery->the_post();
        ?>
			<a href="<?php the_permalink() ?>" class="portfolioItemV3">
                <?php if ( has_post_thumbnail() ) { ?>
                    <?php the_post_thumbnail( 'unithumb-portfolioone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                <?php } else { ?>
			        <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-portfolioone.png' ?>" alt="<?php the_title_attribute() ?>">
                <?php } ?>
				<div class="portfolioItemV3Desc">
					<h3><span><?php the_title() ?></span></h3>
				</div>
			</a>
        <?php
        endwhile;
        endif;
	    wp_reset_postdata();
        ?>
		</div>

        <?php  if ( isset($aCustomData['uni_portfoliothree_text_position'][0]) && $aCustomData['uni_portfoliothree_text_position'][0] == 'bottom' ) { ?>
        <div class="portfolioContentWrap">
            <div class="wrapper">
                <div class="singlePost">
                    <?php the_content() ?>
                </div>  
            </div>
        </div>
        <?php } ?>

        <?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>