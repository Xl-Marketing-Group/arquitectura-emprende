<?php
// Helper functions
require(get_template_directory() . '/includes/helper-functions.php');

// Option tree theme options
define('OT_THEME_VERSION', '2.6.1');

// Filters the Theme Options ID - modded to work with Polylang
function uni_bauhaus_theme_options_id()
{

    $sDefault = 'option_tree';

    if (function_exists('pll_home_url')) {
        if (is_admin() || is_customize_preview()) {
            $user_lang = get_user_meta(get_current_user_id(), 'pll_filter_content', true);
            $selected_lang = filter_input(INPUT_GET, 'lang', FILTER_SANITIZE_STRING);
            $lang = $selected_lang !== null ? $selected_lang : $user_lang;
        } else {
            $lang = pll_current_language();
        }

        if ($lang and $lang != 'all' and $lang != pll_default_language()) {
            return $sDefault . '_' . $lang;
        }
    }

    return $sDefault;
}
add_filter('ot_options_id', 'uni_bauhaus_theme_options_id');

// Filters the Theme Option header list.
function uni_bauhaus_theme_filter_demo_header_list()
{
    echo '<li id="theme-version"><span>Bauhaus WP Theme 1.3.4</span></li>';
}
add_action('ot_header_list', 'uni_bauhaus_theme_filter_demo_header_list');

// Option tree theme options
//add_filter( 'ot_show_pages', '__return_false' );
add_filter('ot_theme_mode', '__return_true');
add_filter('ot_show_options_ui', '__return_false');
add_filter('ot_show_new_layout', '__return_false');
add_filter('ot_show_docs', '__return_false');
add_filter('ot_use_theme_options', '__return_true');
require(get_template_directory() . '/includes/theme-options.php');
require(get_template_directory() . '/option-tree/ot-loader.php');
require(get_template_directory() . '/includes/uni-metabox.php');

// Customizer additions.
require(get_template_directory() . '/includes/customizer.php');

// $content_width
if (! isset($content_width)) {
    $content_width = 1170;
}

// after setup of the child theme
add_action('after_setup_theme', 'uni_bauhaus_theme_setup');
function uni_bauhaus_theme_setup()
{

    // Enable featured image
    add_theme_support('post-thumbnails');

    // Add default posts and comments RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add html5 suppost for search form and comments list
    add_theme_support('html5', array( 'search-form', 'comment-form', 'comment-list' ));

    // translation files for the child theme
    load_child_theme_textdomain('bauhaus', get_template_directory() . '/languages');

    add_theme_support('uni-custom-logo-a', array(
        'height'      => ( ( ot_get_option('uni_logo_height') ) ? ot_get_option('uni_logo_height') : 20 ),
        'width'       => ( ( ot_get_option('uni_logo_width') ) ? ot_get_option('uni_logo_width') : 20 ),
        'flex-height' => false,
        'flex-width'  => false,
        'header-text' => array( 'site-title', 'site-description' ),
    ));

    add_theme_support('uni-custom-logo-b', array(
        'height'      => ( ( ot_get_option('uni_logo_height') ) ? ot_get_option('uni_logo_height') : 20 ),
        'width'       => ( ( ot_get_option('uni_logo_width') ) ? ot_get_option('uni_logo_width') : 20 ),
        'flex-height' => false,
        'flex-width'  => false,
        'header-text' => array( 'site-title', 'site-description' ),
    ));

    add_theme_support('uni-custom-logo-c', array(
        'height'      => ( ( ot_get_option('uni_logo_height') ) ? ot_get_option('uni_logo_height') : 20 ),
        'width'       => ( ( ot_get_option('uni_logo_width') ) ? ot_get_option('uni_logo_width') : 20 ),
        'flex-height' => false,
        'flex-width'  => false,
        'header-text' => array( 'site-title', 'site-description' ),
    ));

    // Indicate widget sidebars can use selective refresh in the Customizer.
    add_theme_support('customize-selective-refresh-widgets');
}

// map styles
function uni_bauhaus_theme_map_styles()
{

    $aArray = array(
        array(
            'value'       => 'bauhausShadesOfGrey',
            'label'       => 'Shades of Grey',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausCartoon',
            'label'       => 'Cartoon',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausGrey',
            'label'       => 'Grey Scale (dafault)',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausBlackWhite',
            'label'       => 'Black & White',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausRetro',
            'label'       => 'Retro',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausNight',
            'label'       => 'Night',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausNightLight',
            'label'       => 'Night Light',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausPapiro',
            'label'       => 'Papiro',
            'src'         => ''
        ),
        array(
            'value'       => 'bauhausDefaultGoogleMap',
            'label'       => 'Google Map Standrard',
            'src'         => ''
        )
    );

    return $aArray;
}

// Register Custom Menu Function
if (function_exists('register_nav_menus')) {
        register_nav_menus(array(
            'primary' => esc_html__('Bauhaus Main Menu', 'bauhaus')
        ));
}

// Menu fallback
function uni_bauhaus_theme_nav_fallback()
{
    $sOutput = '<ul class="mainmenu clear">';
    $sOutput .= wp_list_pages(array('title_li' => '', 'echo' => false));
    $sOutput .= '</ul>';
    echo $sOutput;
}

// Menu mobile fallback
function uni_bauhaus_theme_nav_mobile_fallback()
{
    $sOutput = '<div class="mobileMenu"><ul>';
    $sOutput .= wp_list_pages(array('title_li' => '', 'echo' => false));
    $sOutput .= '</ul></div>';
    echo $sOutput;
}

// add wp site icon fallback
add_filter('wp_head', 'uni_bauhaus_theme_site_icon_fallback', 17);
function uni_bauhaus_theme_site_icon_fallback()
{
    if (! function_exists('has_site_icon') || ! has_site_icon()) {
        echo '<link rel="shortcut icon" href="' . esc_url(get_stylesheet_directory_uri()) . '/favicon.ico" type="image/x-icon" />';
        echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="' . esc_url(get_stylesheet_directory_uri()) . '/apple-touch-icon-144x144.png" />';
        echo '<link rel="apple-touch-icon-precomposed" sizes="152x152" href="' . esc_url(get_stylesheet_directory_uri()) . '/apple-touch-icon-152x152.png" />';
        echo '<link rel="icon" type="image/png" href="' . esc_url(get_stylesheet_directory_uri()) . '/favicon-32x32.png" sizes="32x32" />';
        echo '<link rel="icon" type="image/png" href="' . esc_url(get_stylesheet_directory_uri()) . '/favicon-16x16.png" sizes="16x16" />';
    }
}

// wp-title
add_theme_support('title-tag');
if (! function_exists('_wp_render_title_tag')) :
    function uni_bauhaus_theme_render_title()
    {
        ?>
<title><?php wp_title('-', true, 'right'); ?></title>
        <?php
    }
    add_action('wp_head', 'uni_bauhaus_theme_render_title');
endif;

// TGM class 2.5.0 - neccessary plugins
include(get_template_directory() . '/includes/class-tgm-plugin-activation.php');

add_action('tgmpa_register', 'uni_bauhaus_theme_register_required_plugins');
function uni_bauhaus_theme_register_required_plugins()
{

    $plugins = array(
        array(
            'name'      => 'Instagram Feed',
            'slug'      => 'instagram-feed',
            'required'  => true,
        ),
        array(
            'name'               => 'Envato Market',
            'slug'               => 'envato-market',
            'source'             => get_template_directory() . '/includes/plugins/envato-market.zip',
            'required'           => false,
            'version'            => '1.0.0-RC2',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => 'http://envato.github.io/wp-envato-market/'
        ),
        array(
            'name'               => 'Profilini - Avatar and Profile Manager',
            'slug'               => 'uni-profilini',
            'source'             => get_template_directory() . '/includes/plugins/uni-profilini.zip',
            'required'           => false,
            'version'            => '2.0.0-beta',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        array(
            'name'               => 'Uni Custom Post Types and Taxonomies',
            'slug'               => 'uni-cpt-and-tax',
            'source'             => get_template_directory() . '/includes/plugins/uni-cpt-and-tax.zip',
            'required'           => true,
            'version'            => '1.3.2',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        array(
            'name'               => 'Uni Sortable Users',
            'slug'               => 'uni-sortable-users',
            'source'             => get_template_directory() . '/includes/plugins/uni-sortable-users.zip',
            'required'           => false,
            'version'            => '1.0.0',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => false,
        ),
        array(
            'name'      => 'Intuitive Custom Post Order',
            'slug'      => 'intuitive-custom-post-order',
            'required'  => false,
        ),
        array(
            'name'      => 'Shortcodes Ultimate',
            'slug'      => 'shortcodes-ultimate',
            'required'  => false,
        )
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',
        'strings'      => array(
            'page_title'                      => esc_html__('Install Required Plugins', 'bauhaus'),
            'menu_title'                      => esc_html__('Install Plugins', 'bauhaus'),
            'installing'                      => esc_html__('Installing Plugin: %s', 'bauhaus'), // %s = plugin name.
            'oops'                            => esc_html__('Something went wrong with the plugin API.', 'bauhaus'),
            'notice_can_install_required'     => _n_noop(
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop(
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop(
                'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop(
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_ask_to_update_maybe'      => _n_noop(
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop(
                'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop(
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop(
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop(
                'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
                'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
                'bauhaus'
            ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'bauhaus'
            ),
            'update_link'                     => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'bauhaus'
            ),
            'activate_link'                   => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'bauhaus'
            ),
            'return'                          => esc_html__('Return to Required Plugins Installer', 'bauhaus'),
            'plugin_activated'                => esc_html__('Plugin activated successfully.', 'bauhaus'),
            'activated_successfully'          => esc_html__('The following plugin was activated successfully:', 'bauhaus'),
            'plugin_already_active'           => esc_html__('No action taken. Plugin %1$s was already active.', 'bauhaus'),  // %1$s = plugin name(s).
            'plugin_needs_higher_version'     => esc_html__('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'bauhaus'),  // %1$s = plugin name(s).
            'complete'                        => esc_html__('All plugins installed and activated successfully. %1$s', 'bauhaus'), // %s = dashboard link.
            'contact_admin'                   => esc_html__('Please contact the administrator of this site for help.', 'bauhaus'),

            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        ),
    );

    tgmpa($plugins, $config);
}

// Load necessary theme scripts and styles
function uni_bauhaus_theme_scripts()
{

    $sLocale = get_locale();
    $aLocale = explode('_', $sLocale);
    $sLangCode = $aLocale[0];

    // Load the html5 shiv.
    wp_enqueue_script('html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3');
    wp_script_add_data('html5', 'conditional', 'lt IE 9');
    //
    wp_enqueue_script('jquery-masonry');
    //
    wp_enqueue_script('imagesloaded');
    // bxSlider
    wp_enqueue_script('jquery-bxslider-min', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array('jquery'), '4.2.5');
    // BlackAndWhite
    wp_enqueue_script('jquery-blackandwhite', get_template_directory_uri() . '/js/jquery.BlackAndWhite.js', array('jquery'), '4.1.2');
    // FancyBox
    wp_enqueue_script('jquery-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array('jquery'), '2.1.5');
    // isotope
    wp_enqueue_script('isotope-min', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '2.2.2');
    // Mousewheel
    wp_enqueue_script('jquery-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.js', array('jquery'), '3.1.9');
    // jscrollpane
    wp_enqueue_script('jquery-jscrollpane-min', get_template_directory_uri() . '/js/jquery.jscrollpane.min.js', array('jquery'), '2.0.19');
    // dotdotdot
    wp_enqueue_script('jquery-dotdotdot-min', get_template_directory_uri() . '/js/jquery.dotdotdot.min.js', array('jquery'), '1.2.2');
    // jquery.blockUI
    wp_enqueue_script('jquery-blockui', get_template_directory_uri() . '/js/jquery.blockUI.js', array('jquery'), '2.70.0');
    // parsley
    wp_enqueue_script('jquery-parsley', get_template_directory_uri() . '/js/parsley.min.js', array('jquery'), '2.3.11');
    // parsley localization
    wp_enqueue_script('parsley-localization', get_template_directory_uri() . '/js/parsley/i18n/en.js', array('jquery-parsley'), '2.3.11');
    // Bauhaus scripts
    wp_enqueue_script('uni-bauhaus-theme-script', get_template_directory_uri() . '/js/script.js', array('jquery', 'jquery-masonry', 'jquery-bxslider-min',
        'jquery-blackandwhite', 'jquery-fancybox', 'imagesloaded', 'isotope-min', 'jquery-mousewheel', 'jquery-jscrollpane-min',
        'jquery-dotdotdot-min', 'jquery-blockui', 'jquery-parsley'), '1.3.4');

    if (is_home()) {
        $params = array(
            'site_url'      => esc_url(home_url('/')),
            'ajax_url'      => esc_url(admin_url('admin-ajax.php')),
            'is_home'       => 'yes',
            'locale'        => esc_attr($sLangCode),
            'error_msg'     => esc_html__('Error!', 'bauhaus')
        );
    } else {
        $params = array(
            'site_url'      => esc_url(home_url('/')),
            'ajax_url'      => esc_url(admin_url('admin-ajax.php')),
            'is_home'       => 'no',
            'locale'        => esc_attr($sLangCode),
            'error_msg'     => esc_html__('Error!', 'bauhaus')
        );
    }

    wp_localize_script('uni-bauhaus-theme-script', 'uni_bauhaus_theme_var', $params);

        // parsley localization
        $aParsleyStrings = apply_filters('uni_bauhaus_theme_parsley_strings_filter', array(
            'defaultMessage'    => esc_html__("This value seems to be invalid.", 'bauhaus'),
            'type_email'        => esc_html__("This value should be a valid email.", 'bauhaus'),
            'type_url'          => esc_html__("This value should be a valid url.", 'bauhaus'),
            'type_number'       => esc_html__("This value should be a valid number.", 'bauhaus'),
            'type_digits'       => esc_html__("This value should be digits.", 'bauhaus'),
            'type_alphanum'     => esc_html__("This value should be alphanumeric.", 'bauhaus'),
            'type_integer'      => esc_html__("This value should be a valid integer.", 'bauhaus'),
            'notblank'          => esc_html__("This value should not be blank.", 'bauhaus'),
            'required'          => esc_html__("This value is required.", 'bauhaus'),
            'pattern'           => esc_html__("This value seems to be invalid.", 'bauhaus'),
            'min'               => esc_html__("This value should be greater than or equal to %s.", 'bauhaus'),
            'max'               => esc_html__("This value should be lower than or equal to %s.", 'bauhaus'),
            'range'             => esc_html__("This value should be between %s and %s.", 'bauhaus'),
            'minlength'         => esc_html__("This value is too short. It should have %s characters or more.", 'bauhaus'),
            'maxlength'         => esc_html__("This value is too long. It should have %s characters or fewer.", 'bauhaus'),
            'length'            => esc_html__("This value length is invalid. It should be between %s and %s characters long.", 'bauhaus'),
            'mincheck'          => esc_html__("You must select at least %s choices.", 'bauhaus'),
            'maxcheck'          => esc_html__("You must select %s choices or fewer.", 'bauhaus'),
            'check'             => esc_html__("You must select between %s and %s choices.", 'bauhaus'),
            'equalto'           => esc_html__("This value should be the same.", 'bauhaus'),
            'dateiso'           => esc_html__("This value should be a valid date (YYYY-MM-DD).", 'bauhaus'),
            'minwords'          => esc_html__("This value is too short. It should have %s words or more.", 'bauhaus'),
            'maxwords'          => esc_html__("This value is too long. It should have %s words or fewer.", 'bauhaus'),
            'words'             => esc_html__("This value length is invalid. It should be between %s and %s words long.", 'bauhaus'),
            'gt'                => esc_html__("This value should be greater.", 'bauhaus'),
            'gte'               => esc_html__("This value should be greater or equal.", 'bauhaus'),
            'lt'                => esc_html__("This value should be less.", 'bauhaus'),
            'lte'               => esc_html__("This value should be less or equal.", 'bauhaus'),
            'notequalto'        => esc_html__("This value should be different.", 'bauhaus')
            ));

        wp_localize_script('jquery-parsley', 'uni_bauhaus_theme_parsley_loc', $aParsleyStrings);
}
add_action('wp_enqueue_scripts', 'uni_bauhaus_theme_scripts');

// add google maps js api 3 script
function uni_bauhaus_theme_enqueue_google_maps_script()
{

    $sApiKey = ot_get_option('uni_gmaps_api_key');
    if (ot_get_option('uni_gmaps_elsewhere_enable') != 'on'
        && (
            is_page_template('templ-contact.php') || is_page_template('templ-home.php')
        ) ) {
        if (!empty($sApiKey)) {
            wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js?key='.esc_attr($sApiKey));
        } else {
            wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js', array(), '');
        }
    } else if (ot_get_option('uni_gmaps_elsewhere_enable') == 'on') {
        if (!empty($sApiKey)) {
            wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js?key='.esc_attr($sApiKey));
        } else {
            wp_enqueue_script('google-maps', '//maps.googleapis.com/maps/api/js', array(), '');
        }
    }
}
add_action('wp_enqueue_scripts', 'uni_bauhaus_theme_enqueue_google_maps_script');

// Enqueue style.css (default WordPress stylesheet)
function uni_bauhaus_theme_styles()
{

    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0');

    $ot_set_google_fonts  = get_theme_mod('ot_set_google_fonts', array());

    if (!ot_get_option('uni_google_fonts') || empty($ot_set_google_fonts)) {
        wp_enqueue_style('uni-bauhaus-theme-fonts', uni_bauhaus_theme_fonts_url(), array(), '1.3.4');
    }

    wp_enqueue_style('ball-clip-rotate-style', get_template_directory_uri() . '/css/ball-clip-rotate.css', '0.1.0');

    wp_enqueue_style('bxslider-style', get_template_directory_uri() . '/css/bxslider.css', '4.2.3');

    wp_enqueue_style('fancybox-style', get_template_directory_uri() . '/css/fancybox.css', '2.1.5');

    wp_enqueue_style('jscrollpane-style', get_template_directory_uri() . '/css/jscrollpane.css', '2.1.5');

    wp_enqueue_style('unitheme-styles', get_template_directory_uri() . '/style.css', array('ball-clip-rotate-style', 'bxslider-style',
    'fancybox-style', 'jscrollpane-style'), '1.3.4', 'all');

    wp_enqueue_style('unitheme-adaptive', get_template_directory_uri() . '/css/adaptive.css', array('unitheme-styles'), '1.3.4', 'screen');
}
add_action('wp_enqueue_scripts', 'uni_bauhaus_theme_styles');

//
function uni_bauhaus_theme_fonts_url()
{
    $fonts_url = '';

    $ot_set_google_fonts  = get_theme_mod('ot_set_google_fonts', array());

    if (empty($ot_set_google_fonts)) {
        $font_families = array();
        $font_families[] = 'Montserrat:700,regular';
        $font_families[] = 'Open Sans:300,700,regular';

        $query_args = array(
            'family' => urlencode(implode('|', $font_families)),
            'subset' => urlencode('latin,latin-ext'),
        );

        $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
    }
    return esc_url_raw($fonts_url);
}

//
add_action('wp_head', 'uni_bauhaus_theme_custom_fonts_customisation');
function uni_bauhaus_theme_custom_fonts_customisation()
{
    // helps customize fonts
    if (ot_get_option('uni_google_fonts')) {
        $aCustomFonts           = ot_get_option('uni_google_fonts');
        $ot_google_fonts        = get_theme_mod('ot_google_fonts', array());
        $ot_set_google_fonts    = get_theme_mod('ot_set_google_fonts', array());
        $sFontNameOne = $sFontNameTwo = '';
        if (!empty($ot_set_google_fonts)) {
            if (isset($aCustomFonts[0]) && !empty($aCustomFonts[0]) && isset($ot_google_fonts[$aCustomFonts[0]["family"]])) {
                $sFontNameOne = $ot_google_fonts[$aCustomFonts[0]["family"]]['family'];
            }
            if (isset($aCustomFonts[1]) && !empty($aCustomFonts[1]) && isset($ot_google_fonts[$aCustomFonts[1]["family"]])) {
                $sFontNameTwo = $ot_google_fonts[$aCustomFonts[1]["family"]]['family'];
            }

            if ($sFontNameOne != 'Montserrat' || $sFontNameTwo != 'Open Sans') {
                ?>
<style type="text/css">
/* regular text */
body, table, input, textarea, select, li, button, p, blockquote, ol, dl, form, pre, th, td, a {
font-family: Arial, sans-serif;}

.sidebar-widget label, .singlePost .su-spoiler-title, .singlePost .su-service-title, .singlePost .su-heading-style-default .su-heading-inner,
.singlePost .su-divider a, .singlePost .su-quote-cite a, .singlePost .su-tabs-nav span, .linkToHome, .linkToHome:visited,
#commentform #submit, .comment-wrapper cite a, .comment-wrapper cite, .logged-in-as a, .logged-in-as a:visited,
.comment-edit-link, .comment-edit-link:visited, .comment-reply-link, .comment-reply-link:visited,
.commentsBox .comment-reply-title a, .commentsBox .comment-reply-title a:visited,
.commentsBox .comments-title, .commentsBox .comment-reply-title, .relatedPosts h3, .postPrev, .postNext, .singlePostTags span, 
.singlePost table th a, .singlePost table th a:visited, .singlePost table th, .singlePost dt, .singlePost blockquote p cite,
.singlePost h1 a, .singlePost h2 a, .singlePost h3 a, .singlePost h4 a, .singlePost h5 a, .singlePost h6 a,
.singlePost h1, .singlePost h2, .singlePost h3, .singlePost h4, .singlePost h5, .singlePost h6, .sidebar-widget h3,
.pagination ul li, .pagination ul li a, .pagination ul li a:visited, .blog2ArchiveItem h3 a, .blog2ArchiveItem h3 a:visited,
.archive .blockTitle + p, .blogArchiveItem h3 a, .blogArchiveItem h3 a:visited, .relatedPostsItem h4 a, .relatedPostsItem h4 a:visited,
.serviceDescText h4, .orderServiceItem, .orderServiceItem:visited, .serviceHead h4 span, .wpcf7-form input[type="submit"], .thm-btnSubmit,
.thm-btnSubmit, .contactItem h4, #sb_instagram .sbi_follow_btn a, #sb_instagram .sbi_header_text p, #sb_instagram .sb_instagram_header a,
.teamItem h4, .blockTitle, .screen1 h2, .portfolioItemV2Desc span, .portfolioItemV2SmallImg span, .filterPanel li a, .filterPanel li a:visited,
.copyright, .sendEmailLink, .sendEmailLink:visited, .homeContactInfoWrap h3, .testimonialAuthor strong, .ourServiceItemContent h4, .homeBlockTitle,
.learnMoreLink, .learnMoreLink:visited, .btn-seeMore, .btn-seeMore:visited, .btn-seeAll, .btn-seeAll:visited, .slideMeta h3,
.seeMoreLink, .seeMoreLink:visited, .homeScreenDesc h1, .homeScreenDesc span, .languageSelect ul li a, .languageSelect ul li a:visited,
.languageSelect span, .header2 .mainmenu li ul li a, .header2 .mainmenu li ul li a:visited, 
.header1 .mainmenu li ul li a, .header1 .mainmenu li ul li a:visited, .mainmenu li a, .teamItemDesc h5, 
.logo, .logo:visited {font-family: '<?php echo esc_attr($sFontNameOne); ?>', '<?php echo esc_attr($sFontNameTwo); ?>';}

.contentLeft, .singlePost .su-slider-slide-title, .singlePost .su-carousel .su-carousel-slide-title, #uni_popup, .contactForm .wpcf7-validation-errors,
.parsley-errors-list li, .page404Content p, .page404Content h1, .singleProjectDescText ol li:before, .singleProjectDescText ul li, .singleProjectDescText ol li,
.singleProjectDescText p, .singleProjectDescItem p, .singleProjectDesc h1, #commentform textarea, #commentform input[type="text"],
.comment-content p a, .comment-content p a:visited, .comment-content p, .comment-awaiting-moderation, .comment-metadata time,
.bypostauthor .comment-wrapper .uni-post-author, .logged-in-as, .relatedPostsItem, .singlePostTags a, .singlePostTags a:visited,
.singlePostTags, .singlePost address, .singlePost table td a, .singlePost table td a:visited, .singlePost table td,
.singlePost .wp-caption-text, .singlePost .gallery-caption, .singlePost dd, .singlePost blockquote p a, .singlePost blockquote p a:visited,
.singlePost blockquote p, .singlePost ul li a, .singlePost ul li a:visited, .singlePost ol li a, .singlePost ol li a:visited,
.singlePost ol li:before, .singlePost ul li, .singlePost ol li,
.singlePost dt a, .singlePost dt a:visited, .singlePost dd a, .singlePost dd a:visited, .singlePost p a, .singlePost p a:visited,
.singlePost, .singlePost p, .tagcloud a, .tagcloud a:visited, #wp-calendar tfoot td a, #wp-calendar tfoot td a:visited,
#wp-calendar tbody td, #wp-calendar thead th, #wp-calendar caption, .sidebar-widget .search-form .search-field,
.sidebar-widget li a, .sidebar-widget li a:visited, .sidebar-widget .menu-item a,
.sidebar-widget .menu-item a:visited, .sidebar-widget .cat-item a, .sidebar-widget .cat-item a:visited,
.sidebar-widget li, .sidebar-widget .cat-item, .sidebar-widget .menu-item,
.sidebar-widget .textwidget, .blog2ArchiveItem p, .postTime, .archiveItemMeta, .singlePostMeta, .categoryLink, .categoryLink:visited,
.blogArchiveItem, .serviceDescText p, .serviceDesc p, .servicePrice p, .wpcf7-form textarea, .formTextarea, .wpcf7-form input:not(.wpcf7-submit), .formInput,
.formTextarea, .formInput, .contactForm .wpcf7-form p, .contactForm p, .orderServiceFormWrap p, .contactItem p, .contactItem p a, .contactItem p a:visited,
.teamItem p, .storyWrap p, .screen1 p, .portfolioItemV3Desc h3, .portfolioItemV2Desc p, .portfolioItemV2Desc h4, .portfolioItemV1Desc h3,
.testimonialItem p, .ourServiceItemContent p, .ourServiceItemContent span, .contactInfo2 p,
.aboutUsDesc h3, .teamItemDesc p, .teamItemDesc div.teamItemDescText, .storyWrap .fcell, .storyWrap .scell, .ourServiceBlockDesc, .storyWrap .fullcell,
.aboutUsDesc p {font-family: '<?php echo esc_attr($sFontNameTwo); ?>';}
</style>
                <?php
            }
        }
    }
}

// order form in the footer
add_filter('wp_footer', 'uni_bauhaus_theme_order_form_outut', 27);
function uni_bauhaus_theme_order_form_outut()
{

    if (is_page_template('templ-service.php')) {
        ?>
    <div id="orderServiceForm" class="orderServiceFormWrap">
        <h3 class="blockTitle"><?php esc_html_e('Order Form', 'bauhaus'); ?></h3>
        <p><?php esc_html_e('Please fill out the form and we\'ll get back to you asap', 'bauhaus'); ?></p>
        <form action="<?php echo esc_url(admin_url('admin-ajax.php')); ?>" method="post" class="clear uni_form">
            <input type="hidden" name="uni_contact_nonce" value="<?php echo wp_create_nonce('uni_nonce') ?>" />
            <input type="hidden" name="action" value="uni_bauhaus_theme_order_form" />

            <input class="formInput userName" type="text" name="uni_contact_name" value="" placeholder="<?php esc_html_e('Name', 'bauhaus'); ?>" data-parsley-required="true" data-parsley-trigger="change focusout submit">
            <input class="formInput userEmail" type="text" name="uni_contact_email" value="" placeholder="<?php esc_html_e('E-mail', 'bauhaus'); ?>" data-parsley-required="true" data-parsley-trigger="change focusout submit" data-parsley-type="email">
            <div class="clear"></div>
            <input class="formInput userSubject" type="text" name="uni_contact_subject" value="" placeholder="<?php esc_html_e('Subject', 'bauhaus'); ?>" data-parsley-required="true" data-parsley-trigger="change focusout submit">
            <textarea class="formTextarea" name="uni_contact_msg" cols="30" rows="10" placeholder="<?php esc_html_e('Message', 'bauhaus'); ?>" data-parsley-required="true" data-parsley-trigger="change focusout submit"></textarea>
            <input class="thm-btnSubmit uni_input_submit" type="button" value="<?php esc_html_e('Send', 'bauhaus'); ?>">
        </form>
    </div>
        <?php
    }
}

//
add_action('uni_bauhaus_theme_header', 'uni_bauhaus_theme_header_div_output', 10);
function uni_bauhaus_theme_header_div_output()
{

    do_action('uni_bauhaus_theme_header_wrapper_before');

        $sHeaderLayout = ( ot_get_option('uni_header_layout') && is_page_template('templ-home.php') ) ? ot_get_option('uni_header_layout') : 'header1';

        $iLogoHeight = ( ot_get_option('uni_logo_height') ) ? intval(ot_get_option('uni_logo_height')) : 20;
        $iLogoWidth = ( ot_get_option('uni_logo_width') ) ? intval(ot_get_option('uni_logo_width')) : 20;
        $iSpanHeight = $iLogoHeight;
        $sLogoStyles = 'height:'.$iLogoHeight.'px;padding-left:'.$iLogoWidth.'px;line-height:'.$iLogoHeight.'px;';
        $iMenuHeight1 = ( ot_get_option('uni_h1_menu_top_margin') ) ? intval(ot_get_option('uni_h1_menu_top_margin')) : 23;
        $iMenuHeight2 = ( ot_get_option('uni_h2_menu_top_margin') ) ? intval(ot_get_option('uni_h2_menu_top_margin')) : 37;
    ?>
    <header class="<?php echo esc_attr($sHeaderLayout); ?> clear">
        <?php
        if ($sHeaderLayout == 'header1') {
            $iMenuHeight = $iMenuHeight1;
            ?>
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(bloginfo('name')) ?>" rel="home" itemprop="url" class="logo clear" style="<?php echo esc_attr($sLogoStyles); ?>">
            <?php
            uni_bauhaus_theme_the_custom_logo_a();
            ?>
            <?php if (ot_get_option('uni_display_blog_title') != 'off') { ?>
            <span<?php if (!empty($iSpanHeight)) {
                ?> style="line-height: <?php echo esc_attr($iSpanHeight) ?>px;"<?php
} ?>><?php bloginfo('name') ?></span>
            <?php } ?>
        </a>
        <?php } else if ($sHeaderLayout == 'header2') {
            $iMenuHeight = $iMenuHeight2;
            ?>
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name')) ?>" rel="home" itemprop="url" class="logo clear" style="<?php echo esc_attr($sLogoStyles); ?>">
            <?php
            uni_bauhaus_theme_the_custom_logo_b();
            uni_bauhaus_theme_the_custom_logo_c();
            ?>
            <?php if (ot_get_option('uni_display_blog_title') != 'off') { ?>
            <span style="line-height: <?php echo esc_attr($iSpanHeight) ?>px;"><?php bloginfo('name') ?></span>
            <?php } ?>
        </a>
        <?php } ?>
        
        <a href="#" title="Menu" class="showMobileMenu">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </a>

        <?php uni_bauhaus_theme_display_lang_select() ?>
        
        <style type="text/css">
            .mainmenu {margin-top: <?php echo esc_attr($iMenuHeight) ?>px!important;}
        </style>
        <?php wp_nav_menu(array( 'container' => '', 'container_class' => '', 'menu_class' => 'mainmenu clear', 'theme_location' => 'primary', 'depth' => 3, 'fallback_cb'=> 'uni_bauhaus_theme_nav_fallback' )); ?>
    </header>
    <?php

    do_action('uni_bauhaus_theme_header_wrapper_after');
}

//
add_action('uni_bauhaus_theme_header_wrapper_before', 'uni_bauhaus_theme_loader', 10);
function uni_bauhaus_theme_loader()
{
    ?>
    <div class="loaderWrap">
        <div class="la-ball-clip-rotate la-dark">
            <div></div>
        </div>
    </div>
    <?php
}

//
function uni_bauhaus_theme_the_custom_logo_a($blog_id = 0)
{
    echo uni_bauhaus_theme_get_custom_logo_a($blog_id);
}

//
function uni_bauhaus_theme_get_custom_logo_a($blog_id = 0)
{
    $html = '';

    if (is_multisite() && (int) $blog_id !== get_current_blog_id()) {
        switch_to_blog($blog_id);
    }

    $sLogoAId = ot_get_option('uni_custom_logo_a');

    // We have a logo. Logo is go.
    if ($sLogoAId) {
        $html .= wp_get_attachment_image($sLogoAId, 'full', false, array(
                'class'    => 'mainLogo uni-custom-logo-a',
                'itemprop' => 'logo',
            ));
    } else {
        $html .= '<img class="mainLogo uni-custom-logo-a" src="'.get_template_directory_uri().'/images/logo.svg" width="20" height="20" alt="'.esc_attr(get_bloginfo('description')).'">';
    }

    if (is_multisite() && ms_is_switched()) {
        restore_current_blog();
    }

    return apply_filters('uni_bauhaus_theme_get_custom_logo_a', $html);
}

//
function uni_bauhaus_theme_the_custom_logo_b($blog_id = 0)
{
    echo uni_bauhaus_theme_get_custom_logo_b($blog_id);
}

//
function uni_bauhaus_theme_get_custom_logo_b($blog_id = 0)
{
    $html = '';

    if (is_multisite() && (int) $blog_id !== get_current_blog_id()) {
        switch_to_blog($blog_id);
    }

    $sLogoAId = ot_get_option('uni_custom_logo_b');

    // We have a logo. Logo is go.
    if ($sLogoAId) {
        $html .= wp_get_attachment_image($sLogoAId, 'full', false, array(
                'class'    => 'logoLight uni-custom-logo-b',
                'itemprop' => 'logo',
            ));
    } else {
        $html .= '<img class="logoLight uni-custom-logo-b" src="'.get_template_directory_uri().'/images/logoLight.svg" width="20" height="20" alt="'.esc_attr(get_bloginfo('description')).'">';
    }

    if (is_multisite() && ms_is_switched()) {
        restore_current_blog();
    }

    return apply_filters('uni_bauhaus_theme_get_custom_logo_b', $html);
}

//
function uni_bauhaus_theme_the_custom_logo_c($blog_id = 0)
{
    echo uni_bauhaus_theme_get_custom_logo_c($blog_id);
}

//
function uni_bauhaus_theme_get_custom_logo_c($blog_id = 0)
{
    $html = '';

    if (is_multisite() && (int) $blog_id !== get_current_blog_id()) {
        switch_to_blog($blog_id);
    }

    $sLogoAId = ot_get_option('uni_custom_logo_c');

    // We have a logo. Logo is go.
    if ($sLogoAId) {
        $html .= wp_get_attachment_image($sLogoAId, 'full', false, array(
                'class'    => 'logoDark uni-custom-logo-c',
                'itemprop' => 'logo',
            ));
    } else {
        $html .= '<img class="logoDark uni-custom-logo-c" src="'.get_template_directory_uri().'/images/logoDark.svg" width="20" height="20" alt="'.esc_attr(get_bloginfo('description')).'">';
    }

    if (is_multisite() && ms_is_switched()) {
        restore_current_blog();
    }

    return apply_filters('uni_bauhaus_theme_get_custom_logo_c', $html);
}

function uni_bauhaus_theme_display_lang_select()
{
    // new functionality - Polylang
    if (function_exists('pll_the_languages') && ot_get_option('uni_polylang_switcher_enable') === 'on') {
        $aLanguages = pll_the_languages(array( 'raw' => 1 ));
        if (!empty($aLanguages)) {
            ?>
        <div class="languageSelect">
            <?php foreach ($aLanguages as $aLanguage) :
                if ($aLanguage['current_lang']) {
                    ?>
            <span><i class="languageIcon"></i> <?php echo esc_html($aLanguage['slug']) ?> <i class="dropDownIcon"></i></span>
                <?php }
            endforeach; ?>
            <ul>
            <?php foreach ($aLanguages as $aLanguage) :
                if (!$aLanguage['current_lang']) { ?>
                <li><a href="<?php echo esc_url($aLanguage['url']) ?>"><?php echo esc_html($aLanguage['slug']) ?></a></li>
                <?php }
            endforeach; ?>
            </ul>
        </div>
            <?php
        }
    }
}

//
add_action('admin_enqueue_scripts', 'uni_bauhaus_theme_admin_script', 10, 1);
function uni_bauhaus_theme_admin_script($hook)
{
    wp_enqueue_script('unitheme-admin-script', get_template_directory_uri() . '/js/uni-admin.js', array('jquery'), '1.3.4');
}

// Register sidebar
add_action('widgets_init', 'uni_bauhaus_theme_sidebars_init');
function uni_bauhaus_theme_sidebars_init()
{
    register_sidebar(array(
        'name' => esc_html__('Main Sidebar', 'bauhaus'),
        'id' => 'sidebar-main',
        'description' => '',
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widgettitle">',
    'after_title'   => '</h3>',
    ));
}

// Add new image sizes
add_image_size('unithumb-relativepost', 260, 174, true);
add_image_size('unithumb-singlepost', 840, 561, true);
add_image_size('unithumb-blogone', 370, 0, false);
add_image_size('unithumb-brand', 160, 160, false);
add_image_size('unithumb-service', 545, 300, true);
add_image_size('unithumb-portfolioone', 455, 305, true);
add_image_size('unithumb-portfoliotwoone', 960, 960, true);
add_image_size('unithumb-portfoliotwotwo', 960, 480, true);
add_image_size('unithumb-portfoliotwothree', 480, 480, true);
add_image_size('unithumb-singleprojectthumb', 198, 132, true);
add_image_size('unithumb-logo', 100, 20, true);
add_image_size('unithumb-servicehome', 456, 456, true);
add_image_size('unithumb-testimonialhome', 120, 120, true);
add_image_size('unithumb-testimonialhomebg', 1366, 550, false);
add_image_size('unithumb-useravatar', 180, 180, true);

// add the new role
add_action('after_switch_theme', 'uni_bauhaus_theme_activation_func', 10);
function uni_bauhaus_theme_activation_func()
{
    add_role('architector', esc_html__('Architector', 'bauhaus'), array('read' => true));
    $instructor = get_role('architector');
    $instructor->add_cap('read');
    $instructor->add_cap('edit_published_posts');
    $instructor->add_cap('upload_files');
    $instructor->add_cap('publish_posts');
    $instructor->add_cap('delete_published_posts');
    $instructor->add_cap('edit_posts');
    $instructor->add_cap('delete_posts');
    update_option('posts_per_page', 9);
    flush_rewrite_rules();
}

// remove the new role on theme deactivation
add_action('switch_theme', 'uni_bauhaus_theme_deactivation_func');
function uni_bauhaus_theme_deactivation_func()
{
    remove_role('architector');
}

// Posts similar by tags with thumbnails
function uni_bauhaus_theme_similar_posts_by_tags()
{

    global $post;
    $oOriginalPost = $post;
    $aTags = wp_get_post_tags($post->ID);

    if (isset($aTags)) {
        $aRelatedTagArray = array();
        foreach ($aTags as $oTag) {
            $aRelatedTagArray[] = $oTag->term_id;
        }

        $aRelatedArgs = array(
            'post_type' => 'post',
            'tag__in' => $aRelatedTagArray,
            'post__not_in' => array($post->ID),
            'posts_per_page' => 3,
            'orderby' => 'rand',
            'ignore_sticky_posts' => 1
        );

        $oRelatedQuery = new wp_query($aRelatedArgs);
        if ($oRelatedQuery->have_posts()) {
            echo '<div class="relatedPosts">
					<h3>'.esc_html__('Related Posts', 'bauhaus').'</h3>
					<div class="relatedPostsWrap clear">';

            while ($oRelatedQuery->have_posts()) {
                $oRelatedQuery->the_post();
                ?>
                        <div class="relatedPostsItem">
                            <a href="<?php the_permalink() ?>" class="relatedPostsThumb" title="<?php the_title_attribute() ?>">
                            <?php if (has_post_thumbnail()) { ?>
                                <?php the_post_thumbnail('unithumb-relativepost', array( 'alt' => the_title_attribute('echo=0') )); ?>
                            <?php } else { ?>
                                <img src="http://placehold.it/260x174" width="260" height="174" alt="<?php the_title_attribute() ?>" />
                            <?php } ?>
                            </a>
                            <h4><a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a></h4>
                <?php
                $aCategories = wp_get_post_terms($post->ID, 'category');
                if ($aCategories && !is_wp_error($aCategories)) :
                    $s = count($aCategories);
                    $i = 1;
                    foreach ($aCategories as $oTerm) {
                        echo '<a href="'.esc_url(get_term_link($oTerm->slug, 'category')).'" class="categoryLink">'.esc_attr($oTerm->name).'</a>';
                        if ($i < $s) {
                            echo ', ';
                        }
                        $i++;
                    }
                endif;
                ?>
                        </div>
            <?php }
            echo '</div>
				</div>';
        }
    }
        $post = $oOriginalPost;
        wp_reset_postdata();
}

// custom excerpt
function uni_bauhaus_theme_excerpt($iLength, $iPostId = '', $bEcho = false, $sMore = null)
{
    if (!empty($iPostId)) {
        $oPost = get_post($iPostId);
    } else {
        global $post;
        $oPost = $post;
    }

    if (null === $sMore) {
        $sMore = esc_html__('&hellip;', 'bauhaus');
    }

    $sContent = $oPost->post_content;
    $sContent = wp_strip_all_tags($sContent);
    $sContent = strip_shortcodes($sContent);
    if ('characters' == _x('words', 'word count: words or characters?', 'bauhaus') && preg_match('/^utf\-?8$/i', get_option('blog_charset'))) {
        $sContent = trim(preg_replace("/[\n\r\t ]+/", ' ', $sContent), ' ');
        preg_match_all('/./u', $sContent, $aWordsArray);
        $aWordsArray = array_slice($aWordsArray[0], 0, $iLength + 1);
        $sep = '';
    } else {
        $aWordsArray = preg_split("/[\n\r\t ]+/", $sContent, $iLength + 1, PREG_SPLIT_NO_EMPTY);
        $sep = ' ';
    }

    if (count($aWordsArray) > $iLength) {
        array_pop($aWordsArray);
        $sContent = implode($sep, $aWordsArray);
        $sContent = $sContent . $sMore;
    } else {
        $sContent = implode($sep, $aWordsArray);
    }
    if ($bEcho) {
        echo '<p>'.esc_html($sContent).'</p>';
    } else {
        return esc_html($sContent);
    }
}

//
function uni_bauhaus_theme_allowed_html_wo_a()
{
    $aAllowedHtml = array(
        'br' => array(),
        'em' => array(),
        'strong' => array(),
    );
    return $aAllowedHtml;
}

//
function uni_bauhaus_theme_allowed_html_with_a()
{
    $aAllowedHtml = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'target' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
    );
    return $aAllowedHtml;
}

function uni_bauhaus_theme_custom_excerpt_length($length)
{
    return 60;
}
add_filter('excerpt_length', 'uni_bauhaus_theme_custom_excerpt_length', 999);

function uni_bauhaus_theme_new_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'uni_bauhaus_theme_new_excerpt_more');

// comments
function uni_bauhaus_theme_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    global $post;
    ?>
    <li id="comment-<?php comment_ID(); ?>" <?php comment_class(empty($args['has_children']) ? '' : 'parent'); ?>>
    <a id="view-comment-<?php comment_ID(); ?>" class="comment-anchor"></a>
    <article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
        <footer class="comment-meta">
            <div class="comment-author vcard">
                <?php if (0 != $args['avatar_size']) {
                    echo get_avatar($comment, $args['avatar_size']);
} ?>
            </div><!-- .comment-author -->

            <div class="reply">
                <?php comment_reply_link(array_merge($args, array(
                    'add_below' => 'div-comment',
                    'depth' => $depth,
                    'max_depth' => $args['max_depth'],
                    'before' => '<div>',
                    'after' => '</div>'
                ))); ?>
            </div><!-- .reply -->
        </footer><!-- .comment-meta -->

        <div class="comment-wrapper">
            <?php
            if ($comment->user_id === $post->post_author) {
                printf('<cite class="fn">%s</cite><span class="uni-post-author">%s</span>', esc_url(get_comment_author_link()), esc_html__('post author', 'bauhaus'));
            } else {
                printf('<cite class="fn">%s</cite>', get_comment_author_link());
            }
            ?>

            <span class="comment-metadata">
                <a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
                    <time datetime="<?php comment_time('c'); ?>">
                        <?php printf(_x('%1$s at %2$s', '1: date, 2: time', 'bauhaus'), get_comment_date(), get_comment_time()); ?>
                    </time>
                </a>
                <?php edit_comment_link(esc_html__('Edit', 'bauhaus'), '<span class="separator">&middot;</span> <span class="edit-link">', '</span>'); ?>
            </span><!-- .comment-metadata -->

            <?php if ('0' == $comment->comment_approved) : ?>
                <p class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'bauhaus'); ?></p>
            <?php endif; ?>

            <div class="comment-content">
                <?php comment_text(); ?>
            </div><!-- .comment-content -->
        </div>
    </article><!-- .comment-body -->
    <?php
}

// Ajax contact form - processing
function uni_bauhaus_theme_contact_form()
{

        $aResult               = array();
        $aResult['message']    = esc_html__('Error!', 'bauhaus');
        $aResult['status']     = 'error';

        $sCustomerName          = ( ( isset($_POST['uni_contact_name']) ) ? esc_sql($_POST['uni_contact_name']) : '' );
        $sCustomerEmail         = ( ( isset($_POST['uni_contact_email']) ) ? esc_sql($_POST['uni_contact_email']) : '' );
        $sCustomerSubject       = ( ( isset($_POST['uni_contact_subject']) ) ? esc_sql($_POST['uni_contact_subject']) : '' );
        $sCustomerMsg           = ( ( isset($_POST['uni_contact_msg']) ) ? esc_sql($_POST['uni_contact_msg']) : '' );
        $iPageId                = ( ( isset($_POST['page_id']) ) ? esc_sql(intval($_POST['page_id'])) : '' );
        $sNonce                 = $_POST['uni_contact_nonce'];
        $sAntiCheat             = $_POST['cheaters_always_disable_js'];

    if (( empty($sAntiCheat) || $sAntiCheat != 'true_bro' ) || !wp_verify_nonce($_POST['uni_contact_nonce'], 'uni_nonce')) {
        wp_send_json($aResult);
    }

    if ($sCustomerName && $sCustomerEmail && $sCustomerSubject && $sCustomerMsg) {
        if (!empty($iPageId)) {
            $aPostCustom = get_post_custom($iPageId);
            if (!empty($aPostCustom['uni_contact_email'][0])) {
                $sToEmail = sanitize_email($aPostCustom['uni_contact_email'][0]);
            } else if (ot_get_option('uni_email') && empty($aPostCustom['uni_contact_email'][0])) {
                $sToEmail = sanitize_email(ot_get_option('uni_email'));
            } else {
                $sToEmail = sanitize_email(get_bloginfo('admin_email'));
            }
        } else {
            $sToEmail       = ( ot_get_option('uni_email') ) ? sanitize_email(ot_get_option('uni_email')) : sanitize_email(get_bloginfo('admin_email'));
        }

        if (ot_get_option('uni_use_visitor_email_from') !== 'off') {
            $aHeadersText[]         = "From: $sCustomerName <$sCustomerEmail>";
        } else {
            $sFromEmail             = sanitize_email(ot_get_option('uni_custom_email_from'));
            $aHeadersText[]         = "From: $sCustomerName <$sFromEmail>";
            $aHeadersText[]         = "Sender: $sFromEmail";
            $aHeadersText[]         = "Reply-To: $sCustomerName <$sCustomerEmail>";
        }
        $sSubjectText       = $sCustomerSubject;

        $sBlogName          = get_bloginfo('name');

        $sMessage          =
                "<h3>".sprintf(esc_html__('You have a new message sent from "%s"!', 'bauhaus'), $sBlogName)."</h3>
                    <p></p>
                    <p><strong>".esc_html__('Contact information', 'bauhaus').":</strong><br>
                    ".sprintf(esc_html__('Name: %s', 'bauhaus'), $sCustomerName)."
                    <br>
                    ".sprintf(esc_html__('Email: %s', 'bauhaus'), $sCustomerEmail)."
                    <br>
                    ".esc_html__('Message', 'bauhaus').":
                    <br>$sCustomerMsg
                    </p>";
        $sMessage = stripslashes_deep($sMessage);

        uni_bauhaus_theme_send_email_wrapper($sToEmail, $aHeadersText, $sSubjectText, false, array(), $sMessage);

        $aResult['status']     = 'success';
        $aResult['message']    = esc_html__('Thanks! Your message has been sent!', 'bauhaus');
    } else {
        $aResult['message']    = esc_html__('All fields are required!', 'bauhaus');
    }

        wp_send_json($aResult);
}
add_action('wp_ajax_uni_bauhaus_theme_contact_form', 'uni_bauhaus_theme_contact_form');
add_action('wp_ajax_nopriv_uni_bauhaus_theme_contact_form', 'uni_bauhaus_theme_contact_form');

// Ajax order form - processing
function uni_bauhaus_theme_order_form()
{

        $aResult               = array();
        $aResult['message']    = esc_html__('Error!', 'bauhaus');
        $aResult['status']     = 'error';

        $sCustomerName          = ( ( isset($_POST['uni_contact_name']) ) ? esc_sql($_POST['uni_contact_name']) : '' );
        $sCustomerEmail         = ( ( isset($_POST['uni_contact_email']) ) ? esc_sql($_POST['uni_contact_email']) : '' );
        $sCustomerSubject       = ( ( isset($_POST['uni_contact_subject']) ) ? esc_sql($_POST['uni_contact_subject']) : '' );
        $sCustomerMsg           = ( ( isset($_POST['uni_contact_msg']) ) ? esc_sql($_POST['uni_contact_msg']) : '' );
        $sNonce                 = $_POST['uni_contact_nonce'];
        $sAntiCheat             = $_POST['cheaters_always_disable_js'];

    if (( empty($sAntiCheat) || $sAntiCheat != 'true_bro' ) || !wp_verify_nonce($_POST['uni_contact_nonce'], 'uni_nonce')) {
        wp_send_json($aResult);
    }

    if ($sCustomerName && $sCustomerEmail && $sCustomerSubject && $sCustomerMsg) {
        $sToEmail           = ( ot_get_option('uni_email') ) ? sanitize_email(ot_get_option('uni_email')) : sanitize_email(get_bloginfo('admin_email'));
        if (ot_get_option('uni_use_visitor_email_from') !== 'off') {
            $aHeadersText[]         = "From: $sCustomerName <$sCustomerEmail>";
        } else {
            $sFromEmail             = sanitize_email(ot_get_option('uni_custom_email_from'));
            $aHeadersText[]         = "From: $sCustomerName <$sFromEmail>";
            $aHeadersText[]         = "Sender: $sFromEmail";
            $aHeadersText[]         = "Reply-To: $sCustomerName <$sCustomerEmail>";
        }
        $sSubjectText       = $sCustomerSubject;

        $sBlogName          = get_bloginfo('name');

        $sMessage          =
                "<h3>".sprintf(esc_html__('You have a new order sent from "%s"!', 'bauhaus'), $sBlogName)."</h3>
                    <p></p>
                    <p><strong>".esc_html__('Contact information', 'bauhaus').":</strong><br>
                    ".sprintf(esc_html__('Name: %s', 'bauhaus'), $sCustomerName)."
                    <br>
                    ".sprintf(esc_html__('Email: %s', 'bauhaus'), $sCustomerEmail)."
                    <br>
                    ".sprintf(esc_html__('Subject: %s', 'bauhaus'), $sCustomerSubject)."
                    <br>
                    ".esc_html__('Message', 'bauhaus').":
                    <br>$sCustomerMsg
                    </p>";
        $sMessage = stripslashes_deep($sMessage);

        uni_bauhaus_theme_send_email_wrapper($sToEmail, $aHeadersText, $sSubjectText, false, array(), $sMessage);

        $aResult['status']     = 'success';
        $aResult['message']    = esc_html__('Thanks! Your message has been sent!', 'bauhaus');
    } else {
        $aResult['message']    = esc_html__('All fields are required!', 'bauhaus');
    }

        wp_send_json($aResult);
}
add_action('wp_ajax_uni_bauhaus_theme_order_form', 'uni_bauhaus_theme_order_form');
add_action('wp_ajax_nopriv_uni_bauhaus_theme_order_form', 'uni_bauhaus_theme_order_form');

// fix
if (function_exists('gambit_otf_regen_thumbs_media_downsize')) {
    remove_filter('image_downsize', 'gambit_otf_regen_thumbs_media_downsize', 10, 3);
    add_filter('image_downsize', 'uni_bauhaus_theme_otf_regen_thumbs_media_downsize', 10, 3);
    /**
     * The downsizer. This only does something if the existing image size doesn't exist yet.
     *
     * @param   $out boolean false
     * @param   $id int Attachment ID
     * @param   $size mixed The size name, or an array containing the width & height
     * @return  mixed False if the custom downsize failed, or an array of the image if successful
     */
    function uni_bauhaus_theme_otf_regen_thumbs_media_downsize($out, $id, $size)
    {

        if (!in_array($size, array( 'unithumb-brand' ))) {
        // Gather all the different image sizes of WP (thumbnail, medium, large) and,
        // all the theme/plugin-introduced sizes.
            global $_gambit_otf_regen_thumbs_all_image_sizes;
            if (! isset($_gambit_otf_regen_thumbs_all_image_sizes)) {
                global $_wp_additional_image_sizes;

                $_gambit_otf_regen_thumbs_all_image_sizes = array();
                $interimSizes = get_intermediate_image_sizes();

                foreach ($interimSizes as $sizeName) {
                    if (in_array($sizeName, array( 'thumbnail', 'medium', 'large' ))) {
                        $_gambit_otf_regen_thumbs_all_image_sizes[ $sizeName ]['width'] = get_option($sizeName . '_size_w');
                        $_gambit_otf_regen_thumbs_all_image_sizes[ $sizeName ]['height'] = get_option($sizeName . '_size_h');
                        $_gambit_otf_regen_thumbs_all_image_sizes[ $sizeName ]['crop'] = (bool) get_option($sizeName . '_crop');
                    } elseif (isset($_wp_additional_image_sizes[ $sizeName ])) {
                        $_gambit_otf_regen_thumbs_all_image_sizes[ $sizeName ] = $_wp_additional_image_sizes[ $sizeName ];
                    }
                }
            }

        // This now contains all the data that we have for all the image sizes
            $allSizes = $_gambit_otf_regen_thumbs_all_image_sizes;

        // If image size exists let WP serve it like normally
            $imagedata = wp_get_attachment_metadata($id);

        // Image attachment doesn't exist
            if (! is_array($imagedata)) {
                return false;
            }

        // If the size given is a string / a name of a size
            if (is_string($size)) {
                // If WP doesn't know about the image size name, then we can't really do any resizing of our own
                if (empty($allSizes[ $size ])) {
                    return false;
                }

                // If the size has already been previously created, use it
                if (! empty($imagedata['sizes'][ $size ]) && ! empty($allSizes[ $size ])) {
                    // But only if the size remained the same
                    if ($allSizes[ $size ]['width'] == $imagedata['sizes'][ $size ]['width']
                    && $allSizes[ $size ]['height'] == $imagedata['sizes'][ $size ]['height'] ) {
                        return false;
                    }

                    // Or if the size is different and we found out before that the size really was different
                    if (! empty($imagedata['sizes'][ $size ][ 'width_query' ])
                    && ! empty($imagedata['sizes'][ $size ]['height_query']) ) {
                        if ($imagedata['sizes'][ $size ]['width_query'] == $allSizes[ $size ]['width']
                        && $imagedata['sizes'][ $size ]['height_query'] == $allSizes[ $size ]['height'] ) {
                            return false;
                        }
                    }
                }

                // Resize the image
                $resized = image_make_intermediate_size(
                    get_attached_file($id),
                    $allSizes[ $size ]['width'],
                    $allSizes[ $size ]['height'],
                    $allSizes[ $size ]['crop']
                );

                // Resize somehow failed
                if (! $resized) {
                    return false;
                }

                // Save the new size in WP
                $imagedata['sizes'][ $size ] = $resized;

                // Save some additional info so that we'll know next time whether we've resized this before
                $imagedata['sizes'][ $size ]['width_query'] = $allSizes[ $size ]['width'];
                $imagedata['sizes'][ $size ]['height_query'] = $allSizes[ $size ]['height'];

                wp_update_attachment_metadata($id, $imagedata);

                // Serve the resized image
                $att_url = wp_get_attachment_url($id);
                return array( dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true );


            // If the size given is a custom array size
            } else if (is_array($size)) {
                $imagePath = get_attached_file($id);

                // This would be the path of our resized image if the dimensions existed
                $imageExt = pathinfo($imagePath, PATHINFO_EXTENSION);
                $imagePath = preg_replace('/^(.*)\.' . $imageExt . '$/', sprintf('$1-%sx%s.%s', $size[0], $size[1], $imageExt), $imagePath);

                $att_url = wp_get_attachment_url($id);

                // If it already exists, serve it
                if (file_exists($imagePath)) {
                    return array( dirname($att_url) . '/' . basename($imagePath), $size[0], $size[1], true );
                }

                // If not, resize the image...
                $resized = image_make_intermediate_size(
                    get_attached_file($id),
                    $size[0],
                    $size[1],
                    true
                );

                // Get attachment meta so we can add new size
                $imagedata = wp_get_attachment_metadata($id);

                // Save the new size in WP so that it can also perform actions on it
                $imagedata['sizes'][ $size[0] . 'x' . $size[1] ] = $resized;
                wp_update_attachment_metadata($id, $imagedata);

                // Resize somehow failed
                if (! $resized) {
                    return false;
                }

                // Then serve it
                return array( dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true );
            }

            return false;
        }
    }
}

function uni_bauhaus_theme_sort_project_terms($a, $b)
{
    return $a['order'] - $b['order'];
}

//
header("X-XSS-Protection: 0");
?>
