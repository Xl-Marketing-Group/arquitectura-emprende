<?php
/*
*  Template Name: About Page
*/
get_header();
$aUniAllowedHtmlWoA = uni_bauhaus_theme_allowed_html_wo_a();
$aUniAllowedHtmlWithA = uni_bauhaus_theme_allowed_html_with_a();
$aUniAllowedHtmlCustom = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'target' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
        'p' => array()
    );
?>

	<section class="container">

        <?php if (have_posts()) : while (have_posts()) : the_post();
        $aCustomData = get_post_custom( $post->ID );

        $iPostThumb = get_post_thumbnail_id( $post->ID );
        if ( !empty($iPostThumb) ) $aThumb = wp_get_attachment_image_src( $iPostThumb, 'full' );
        if ( !empty($aThumb) ) {
            $sBackgroundImageUrl = $aThumb[0];
        } else {
            $sBackgroundImageUrl = get_template_directory_uri().'/images/placeholders/bg-about.png';
        }
        ?>
		<div class="screen1" style="background-image: url(<?php echo esc_url( $sBackgroundImageUrl ); ?>);">
			<div class="wrapper">                                     
				<h1><?php if ( !empty($aCustomData['uni_title'][0]) ) echo wp_kses( $aCustomData['uni_title'][0], $aUniAllowedHtmlWoA ); ?></h1>
				<p><?php if ( !empty($aCustomData['uni_sub_title'][0]) ) echo wp_kses( $aCustomData['uni_sub_title'][0], $aUniAllowedHtmlWoA ); ?></p>
			</div>
			<i class="arrowDown"></i>
		</div>
        <?php if ( isset($aCustomData['uni_about_story_enable'][0]) && $aCustomData['uni_about_story_enable'][0] == 'on' ) { ?>
		<div class="ourStory">
			<div class="wrapper">
				<h3 class="blockTitle"><?php if ( !empty($aCustomData['uni_our_story_title'][0]) ) echo wp_kses( $aCustomData['uni_our_story_title'][0], $aUniAllowedHtmlWoA ); ?></h3>
				<div class="storyWrap clear">
                <?php if ( !empty($aCustomData['uni_left_col_text'][0]) && !empty($aCustomData['uni_right_col_text'][0]) ) { ?>
					<div class="fcell">
						<?php if ( !empty($aCustomData['uni_left_col_text'][0]) ) echo wp_kses( $aCustomData['uni_left_col_text'][0], $aUniAllowedHtmlCustom ); ?>
					</div>
					<div class="scell">
						<?php if ( !empty($aCustomData['uni_right_col_text'][0]) ) echo wp_kses( $aCustomData['uni_right_col_text'][0], $aUniAllowedHtmlCustom ); ?>
					</div>
                <?php } else if ( empty($aCustomData['uni_left_col_text'][0]) && !empty($aCustomData['uni_right_col_text'][0]) ) { ?>
					<div class="fullcell">
						<?php echo wp_kses( $aCustomData['uni_right_col_text'][0], $aUniAllowedHtmlCustom ); ?>
					</div>
                <?php } else if ( !empty($aCustomData['uni_left_col_text'][0]) && empty($aCustomData['uni_right_col_text'][0]) ) { ?>
					<div class="fullcell">
						<?php echo wp_kses( $aCustomData['uni_left_col_text'][0], $aUniAllowedHtmlCustom ); ?>
					</div>
                <?php } ?>
				</div>
			</div>
		</div>
        <?php } ?>
        <?php endwhile; endif;
        wp_reset_postdata(); ?>

    <?php if ( isset($aCustomData['uni_about_team_enable'][0]) && $aCustomData['uni_about_team_enable'][0] == 'on' ) { ?>
		<div class="ourTeam">
			<div class="wrapper">
				<h3 class="blockTitle"><?php echo ( !empty($aCustomData['uni_about_team_title'][0]) ) ? esc_html( $aCustomData['uni_about_team_title'][0] ) : esc_html__('Meet Our Team', 'bauhaus'); ?></h3>
    <?php
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if ( is_plugin_active( 'uni-sortable-users/uni-sortable-users.php' ) ) {
        if ( !empty( $aCustomData['uni_meet_team_members'][0] ) ) {
            $aUsersArray = maybe_unserialize($aCustomData['uni_meet_team_members'][0]);
            $oUserQuery = new WP_User_Query( array('role' => 'architector', 'include' => $aUsersArray, 'meta_key' => 'user_order', 'orderby' => 'meta_value_num', 'order' => 'asc') );
        } else {
            $oUserQuery = new WP_User_Query( array('role' => 'architector', 'meta_key' => 'user_order', 'orderby' => 'meta_value_num', 'order' => 'asc') );
        }
    } else {
        if ( !empty( $aCustomData['uni_meet_team_members'][0] ) ) {
            $aUsersArray = maybe_unserialize($aCustomData['uni_meet_team_members'][0]);
            $oUserQuery = new WP_User_Query( array('role' => 'architector', 'include' => $aUsersArray) );
        } else {
            $oUserQuery = new WP_User_Query( array('role' => 'architector') );
        }
    }
    if ( ! empty( $oUserQuery->results ) ) {
    ?>
				<div class="ourTeamWrap">
    <?php
        foreach ( $oUserQuery->results as $oUser ) {
            if ( class_exists( 'Uni_Profilini' ) ) :
                $user_id            = $oUser->ID;
                $profession         = get_user_meta( $user_id, '_uni_profilini_profession', true );
                $phone_number       = get_user_meta( $user_id, '_uni_profilini_phone_number', true );
                $social_icons       = get_user_meta( $user_id, '_uni_profilini_si', true );
                $avatar_attach_id   = get_user_meta( $user_id, '_uni_profilini_avatar_id', true );
                if ( ! empty($avatar_attach_id) && get_post( $avatar_attach_id ) ) {
                    $thumbnail_html = wp_get_attachment_image( $avatar_attach_id, 'unithumb-useravatar', false, array(
                        'alt' => esc_attr( $oUser->display_name)
                    ) );
                } else {
                    $thumbnail_html = '<img src="http://placehold.it/180x180" alt="' . esc_attr( $oUser->display_name ) . '">';
                }
    ?>
					<div class="teamItem" data-userid="user_<?php echo esc_attr( $user_id ) ?>">
                        <?php echo $thumbnail_html ?>
						<h4><?php echo esc_html( $oUser->display_name ); ?></h4>
						<p><?php echo esc_html( $profession ); ?></p>
					</div>

					<div class="teamItemDesc" id="user_<?php echo esc_attr( $user_id ) ?>">
						<div class="teamItemDescWrap">
							<?php echo $thumbnail_html ?>
							<h5><?php echo esc_html( $oUser->display_name ); ?></h5>
	    					<p class="teamItemDescText1">
	    					    <?php echo esc_html( $profession ); ?>
                                <?php echo esc_html( $phone_number ); ?>
                            </p>
	    					<div class="teamItemDescText"><?php echo wp_kses( $oUser->description, $aUniAllowedHtmlWithA ) ?></div>
	    					<div class="teamItemSocial">
                            <?php
                            if ( ! empty( $social_icons ) ) {
                                foreach ( $social_icons as $key => $value ) {
                                    echo '<a href="' . esc_url($value['url']) . '"><i class="' . esc_attr($value['icon']) . '"></i></a>';
                                }
                            }
                            ?>
	    					</div>
						</div>
						<span class="closeTeamDesc">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="1.5 -1 16 16" enable-background="new 1.5 -1 16 16" xml:space="preserve">
								<path fill="#0b0b0b" d="M11.185 7l6.315 6.314L15.814 15L9.5 8.685L3.186 15L1.5 13.314L7.815 7L1.5 0.686L3.186-1L9.5 5.3 L15.814-1L17.5 0.686L11.185 7z"/>
							</svg>
						</span>
					</div>
    <?php
            else :
            $aUserData = ( get_user_meta( $oUser->ID, '_uni_user_data', true ) ) ? get_user_meta( $oUser->ID, '_uni_user_data', true ) : array();
    ?>
					<div class="teamItem" data-userid="user_<?php echo $oUser->ID ?>">
                        <?php echo do_shortcode('[uav-display-avatar id="'.$oUser->ID.'" size="180" alt="'.esc_attr($oUser->display_name).'"]') ?>
						<h4><?php echo esc_html( $oUser->display_name ); ?></h4>
						<p><?php if ( !empty($aUserData['profession']) ) echo esc_attr( $aUserData['profession'] ) ?></p>
					</div>

					<div class="teamItemDesc" id="user_<?php echo $oUser->ID ?>">
						<div class="teamItemDescWrap">
							<?php echo do_shortcode('[uav-display-avatar id="'.$oUser->ID.'" size="180" alt="'.esc_attr($oUser->display_name).'"]') ?>
							<h5><?php echo esc_html( $oUser->display_name ); ?></h5>
	    					<p class="teamItemDescText1"><?php if ( !empty($aUserData['profession']) ) echo esc_attr( $aUserData['profession'] ) ?><?php if ( !empty($aUserData['phone']) ) echo '<br>'.esc_html( $aUserData['phone'] ) ?></p>
	    					<div class="teamItemDescText"><?php echo wp_kses( $oUser->description, $aUniAllowedHtmlWithA ) ?></div>
	    					<div class="teamItemSocial">
	                        <?php if ( !empty($aUserData['social_link_uri_one']) && !empty($aUserData['social_link_icon_one']) ) { ?>
	    						<a href="<?php echo esc_url( $aUserData['social_link_uri_one'] ) ?>"><i class="fa <?php echo esc_attr($aUserData['social_link_icon_one']) ?>"></i></a>
	                        <?php } ?>
	                        <?php if ( !empty($aUserData['social_link_uri_two']) && !empty($aUserData['social_link_icon_two']) ) { ?>
	    						<a href="<?php echo esc_url( $aUserData['social_link_uri_two'] ) ?>"><i class="fa <?php echo esc_attr($aUserData['social_link_icon_two']) ?>"></i></a>
	                        <?php } ?>
	                        <?php if ( !empty($aUserData['social_link_uri_three']) && !empty($aUserData['social_link_icon_three']) ) { ?>
	    						<a href="<?php echo esc_url( $aUserData['social_link_uri_three'] ) ?>"><i class="fa <?php echo esc_attr($aUserData['social_link_icon_three']) ?>"></i></a>
	                        <?php } ?>
	                        <?php if ( !empty($aUserData['social_link_uri_four']) && !empty($aUserData['social_link_icon_four']) ) { ?>
	    						<a href="<?php echo esc_url( $aUserData['social_link_uri_four'] ) ?>"><i class="fa <?php echo esc_attr($aUserData['social_link_icon_four']) ?>"></i></a>
	                        <?php } ?>
	                        <?php if ( !empty($aUserData['social_link_uri_five']) && !empty($aUserData['social_link_icon_five']) ) { ?>
	    						<a href="<?php echo esc_url( $aUserData['social_link_uri_five'] ) ?>"><i class="fa <?php echo esc_attr($aUserData['social_link_icon_five']) ?>"></i></a>
	                        <?php } ?>
	    					</div>
						</div>
						<span class="closeTeamDesc">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="1.5 -1 16 16" enable-background="new 1.5 -1 16 16" xml:space="preserve">
								<path fill="#0b0b0b" d="M11.185 7l6.315 6.314L15.814 15L9.5 8.685L3.186 15L1.5 13.314L7.815 7L1.5 0.686L3.186-1L9.5 5.3 L15.814-1L17.5 0.686L11.185 7z"/>
							</svg>
						</span>
					</div>
    <?php
            endif;
        }
    ?>
				</div>
    <?php
    }
    ?>
			</div>
		</div>
    <?php } ?>

    <?php if ( isset($aCustomData['uni_about_brands_enable'][0]) && $aCustomData['uni_about_brands_enable'][0] == 'on' ) { ?>
		<div class="ourPartners">
			<div class="wrapper">
				<h3 class="blockTitle"><?php echo ( !empty($aCustomData['uni_about_brands_title'][0]) ) ? esc_html( $aCustomData['uni_about_brands_title'][0] ) : esc_html__('We Have Worked With', 'bauhaus'); ?></h3>
				<div class="ourPartnersWrap">
	<?php
	$aBrandsArgs = array(
	    'post_type' => 'uni_brand',
        'posts_per_page' => -1,
	    'orderby' => 'menu_order',
        'order' => 'asc'
	);
	$oBrands = new WP_Query( $aBrandsArgs );
	if ( $oBrands->have_posts() ) :
	while ( $oBrands->have_posts() ) : $oBrands->the_post();
        $aPostCustom = get_post_custom( $post->ID );
	?>
					<div<?php if ( !empty($aPostCustom['uni_brand_uri'][0]) ) echo ' data-href="'.esc_url($aPostCustom['uni_brand_uri'][0]).'"'; ?> class="partnersItem<?php if ( ot_get_option( 'uni_brands_grayscale_enable' ) == 'on' ) { echo ' discolored'; } if ( !empty($aPostCustom['uni_brand_uri'][0]) ) { echo ' brand-with-link'; } ?>">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <?php the_post_thumbnail( 'unithumb-brand', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                        <?php } else { ?>
						    <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/brand.png' ?>" alt="<?php the_title_attribute() ?>" width="140" height="22">
                        <?php } ?>
					</div>
	<?php
	endwhile; endif;
	wp_reset_postdata();
    ?>
				</div>
			</div>
		</div>
    <?php } ?>

    <?php if ( isset($aCustomData['uni_about_instagram_enable'][0]) && $aCustomData['uni_about_instagram_enable'][0] == 'on' ) { ?>
		<div class="ourInstagram">
			<i class="iconInstagam"></i>
            <div class="clear"></div>
            <?php echo do_shortcode('[instagram-feed showheader=true widthunit=272 heightunit=228 imagepadding=0 showfollow=true showbutton=false]'); ?>
        </div>
    <?php } ?>

	</section>

<?php get_footer(); ?>