<?php
/*
*  Template Name: Portfolio Five
*/
get_header(); ?>

	<section class="container">

    <?php if (have_posts()) : while (have_posts()) : the_post();
        $aCustomData = get_post_custom( $post->ID );
    ?>

        <?php if ( isset($aCustomData['uni_portfoliotwo_text_position'][0]) && $aCustomData['uni_portfoliotwo_text_position'][0] == 'top' ) { ?>
		<div class="portfolioContentWrap">
			<div class="wrapper">
				<div class="singlePost">
		            <?php the_content() ?>
		        </div>
			</div>
		</div>
        <?php } ?>

		<div class="portfolio-v2 clear">

        <?php
        $aProjectsChosen = ( !empty($aCustomData['uni_portfoliotwo_projects'][0]) ) ? maybe_unserialize($aCustomData['uni_portfoliotwo_projects'][0]) : array();
        if ( !empty($aProjectsChosen) ) {
            $aProjectArgs = array(
                'post_type' => 'uni_project',
                'posts_per_page' => -1,
                'post__in' => $aProjectsChosen,
                'ignore_sticky_posts' => true,
	            'orderby' => 'menu_order',
                'order' => 'asc'
            );
        } else {
            $aProjectArgs = array(
                'post_type' => 'uni_project',
                'posts_per_page' => -1,
	            'orderby' => 'menu_order',
                'order' => 'asc'
            );
        }

        $oProjectQuery = new wp_query( $aProjectArgs );
        if ( $oProjectQuery->have_posts() ) :
        $i = 1;
        while ( $oProjectQuery->have_posts() ) : $oProjectQuery->the_post();
        ?>

                <?php if ( ( $i == 1 ) || in_array($i, range(5, 9, 4)) ) { ?>
                    <a class="portfolioItemV2 portfolioItemOne" href="<?php the_permalink() ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail( 'unithumb-portfoliotwoone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                    <?php } else { ?>
        			    <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-portfoliotwoone.jpg' ?>" alt="<?php the_title_attribute() ?>">
                    <?php } ?>
                        <div class="portfolioItemOverlay"></div>
                        <div class="portfolioItemV2Desc">
                            <h4><?php the_title() ?></h4>
                            <?php uni_bauhaus_theme_excerpt(30, '', true) ?>
                            <span><?php esc_html_e('view more', 'bauhaus') ?><i></i></span>
                        </div>
                    </a>
                <?php } ?>

                <?php if ( ( $i == 2 ) || in_array($i, range(6, 10, 4)) ) { ?>
                    <a class="portfolioItemV2 portfolioItemTwo" href="<?php the_permalink() ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail( 'unithumb-portfoliotwoone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                    <?php } else { ?>
        			    <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-portfoliotwoone.jpg' ?>" alt="<?php the_title_attribute() ?>">
                    <?php } ?>
                        <div class="portfolioItemOverlay"></div>
                        <div class="portfolioItemV2Desc">
                            <h4><?php the_title() ?></h4>
                            <?php uni_bauhaus_theme_excerpt(30, '', true) ?>
                            <span><?php esc_html_e('view more', 'bauhaus') ?><i></i></span>
                        </div>
                    </a>
                <?php } ?>

                <?php if ( ( $i == 3 ) || in_array($i, range(7, 11, 4)) ) { ?>
                    <a class="portfolioItemV2 portfolioItemThree" href="<?php the_permalink() ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail( 'unithumb-portfoliotwoone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                    <?php } else { ?>
        			    <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-portfoliotwoone.jpg' ?>" alt="<?php the_title_attribute() ?>">
                    <?php } ?>
                        <div class="portfolioItemOverlay"></div>
                        <div class="portfolioItemV2Desc">
                            <h4><?php the_title() ?></h4>
                            <?php uni_bauhaus_theme_excerpt(30, '', true) ?>
                            <span><?php esc_html_e('view more', 'bauhaus') ?><i></i></span>
                        </div>
                    </a>
                <?php } ?>

                <?php if ( ( $i == 4 ) || in_array($i, range(8, 12, 4)) ) { ?>
                    <a class="portfolioItemV2 portfolioItemFour" href="<?php the_permalink() ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail( 'unithumb-portfoliotwoone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                    <?php } else { ?>
        			    <img src="<?php echo esc_url( get_template_directory_uri() ) . '/images/placeholders/unithumb-portfoliotwoone.jpg' ?>" alt="<?php the_title_attribute() ?>">
                    <?php } ?>
                        <div class="portfolioItemOverlay"></div>
                        <div class="portfolioItemV2Desc">
                            <h4><?php the_title() ?></h4>
                            <?php uni_bauhaus_theme_excerpt(30, '', true) ?>
                            <span><?php esc_html_e('view more', 'bauhaus') ?><i></i></span>
                        </div>
                    </a>
                <?php } ?>

        <?php
        $i++;
        endwhile;
        endif;
	    wp_reset_postdata();
        ?>
		</div>

        <?php if ( isset($aCustomData['uni_portfoliotwo_text_position'][0]) && $aCustomData['uni_portfoliotwo_text_position'][0] == 'bottom' ) { ?>
		<div class="clear"></div>
        <div class="portfolioContentWrap">
			<div class="wrapper">
				<div class="singlePost">
		            <?php the_content() ?>
		        </div>	
			</div>
		</div>
        <?php } ?>

        <?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>