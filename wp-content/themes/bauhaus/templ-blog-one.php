<?php
/*
*  Template Name: Blog One
*/
get_header() ?>

	<section class="container">
		<div class="wrapper">
			<div class="blogWrap clear">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $aBlogArgs = array(
            'post_type' => 'post',
            'paged' => $paged
        );

        $oBlogQuery = new wp_query( $aBlogArgs );
        if ( $oBlogQuery->have_posts() ) :
        while ( $oBlogQuery->have_posts() ) : $oBlogQuery->the_post();
        ?>
				<div <?php post_class('blogArchiveItem') ?>>
					<a href="<?php the_permalink() ?>" class="archiveItemThumb" title="<?php the_title_attribute() ?>">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail( 'unithumb-blogone', array( 'alt' => the_title_attribute('echo=0') ) ); ?>
                    <?php } else { ?>
                        <img src="http://placehold.it/370x292" width="370" height="292" alt="<?php the_title_attribute() ?>" />
                    <?php } ?>
                    </a>
					<h3><a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_title() ?></a></h3>
        <?php
        $aCategories = wp_get_post_terms( $post->ID, 'category' );
        if ( $aCategories && !is_wp_error( $aCategories ) ) :
        $s = count($aCategories);
        $i = 1;
	    foreach ( $aCategories as $oTerm ) {
	        echo '<a href="'.esc_url( get_term_link( $oTerm->slug, 'category' ) ).'" class="categoryLink">'.esc_html( $oTerm->name ).'</a>';
            if ($i < $s) echo ', ';
            $i++;
	    }
        endif;
        ?>
				</div>
        <?php
        endwhile;
        endif;
	    wp_reset_postdata();
        ?>
			</div>

			<div class="pagination clear">
    	    <?php
            $big = 999999999;
    		echo paginate_links( array(
    			'base'         => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    			'format'       => '?paged=%#%',
    			'add_args'     => '',
    			'current'      => max( 1, get_query_var( 'paged' ) ),
    			'total'        => $oBlogQuery->max_num_pages,
    			'prev_text'    => '<i>
    								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="&#1057;&#1083;&#1086;&#1081;_1" x="0px" y="0px" width="7px" height="11px" viewBox="0 0 7 11" enable-background="new 0 0 7 11" xml:space="preserve">
    									<path fill="#0B0B0B" class="paginationArrowIcon" d="M0.95 4.636L6.049 0L7 0.864L1.899 5.5L7 10.136L6.049 11L0 5.5L0.95 4.636z"/>
    								</svg>
    							</i>'.esc_html__('previous', 'bauhaus'),
    			'next_text'    => esc_html__('next', 'bauhaus').'<i>
    								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="&#1057;&#1083;&#1086;&#1081;_1" x="0px" y="0px" width="7px" height="11px" viewBox="0 0 7 11" enable-background="new 0 0 7 11" xml:space="preserve">
    									<path fill="#0B0B0B" class="paginationArrowIcon" d="M6.05 6.364L0.951 11L0 10.136L5.102 5.5L0 0.864L0.951 0L7 5.5L6.05 6.364z"/>
    								</svg>
    							</i>',
    			'type'         => 'list',
    			'end_size'     => 3,
    			'mid_size'     => 3
    		) );
    	    ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>