<?php
/**
 * Initialize the custom Meta Boxes.
 */
add_action( 'admin_init', 'unitheme_custom_meta_boxes' );

/**
 * Meta Boxes.
 *
 * @return    void
 * @since     2.0
 */
function unitheme_custom_meta_boxes() {

  $project_meta_box = array(
    'id'          => 'project_meta_box',
    'title'       => esc_html__( 'Parameters for Project', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_project' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Display settings', 'bauhaus' ),
        'id'          => 'tab_project_display',
        'type'        => 'tab'
      ),
      array(
        'id'          => 'uni_project_classic_view_enable',
        'label'       => esc_html__( 'Enable/Disable classic view', 'bauhaus' ),
        'desc'        => esc_html__( 'You can enable a classic view for this page. It will look like a regular page but with an image in the header and wide content area.', 'bauhaus' ),
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_project_classic_title_colour',
        'label'       => esc_html__( 'Colour of page header title', 'bauhaus' ),
        'desc'        => esc_html__( 'Choose a colour for a page header title for classic view of single project page.', 'bauhaus' ),
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
        'section'     => 'tab_colours',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'label'       => esc_html__( 'Slider', 'bauhaus' ),
        'id'          => 'tab_project_slider',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Images for slider', 'bauhaus' ),
        'id'          => 'uni_project_slider',
        'type'        => 'gallery',
        'desc'        => esc_html__( 'Added images will be shown in slider on single project page', 'bauhaus' ),
        'condition'   => ''
      ),
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Portfolio page link', 'bauhaus' ),
        'id'          => 'uni_project_portfolio_uri',
        'type'        => 'text',
        'desc'        => esc_html__( 'You can override "Portfolio page" global option here. If you add an url, it will be used for this project only. Of course, you may add any external URI here.', 'bauhaus' ),
      ),
      array(
        'label'       => esc_html__( 'Project\'s area', 'bauhaus' ),
        'id'          => 'uni_area',
        'type'        => 'text',
        'desc'        => ''
      )
    )
  );

  $price_meta_box = array(
    'id'          => 'price_meta_box',
    'title'       => esc_html__( 'Parameters for Price', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_price' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Price value', 'bauhaus' ),
        'id'          => 'uni_price',
        'type'        => 'text',
        'desc'        => ''
      ),
      array(
        'label'       => esc_html__( 'Currency sign', 'bauhaus' ),
        'id'          => 'uni_currency',
        'type'        => 'text',
        'desc'        => ''
      ),
      array(
        'label'       => esc_html__( 'per... ?', 'bauhaus' ),
        'id'          => 'uni_type',
        'type'        => 'text',
        'desc'        => esc_html__('for example: "unit", "square meter" etc.', 'bauhaus')
      ),
      array(
        'label'       => esc_html__( 'Display/hide "Order" button', 'bauhaus' ),
        'id'          => 'uni_service_display_order_btn',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'Custom label for "Order" button', 'bauhaus' ),
        'id'          => 'uni_service_order_btn_label',
        'type'        => 'text',
        'desc'        => ''
      )
    )
  );

  $testimonial_meta_box = array(
    'id'          => 'testimonial_meta_box',
    'title'       => esc_html__( 'Parameters for Testimonial', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_testimonial' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Testimonial author name', 'bauhaus' ),
        'id'          => 'uni_testimonial_name',
        'type'        => 'text',
        'desc'        => ''
      ),
      array(
        'label'       => esc_html__( 'Testimonial author position', 'bauhaus' ),
        'id'          => 'uni_testimonial_position',
        'type'        => 'text',
        'desc'        => ''
      )
    )
  );

  $about_meta_box = array(
    'id'          => 'about_meta_box',
    'title'       => esc_html__( 'Parameters for "About" page', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'H1 title on the page', 'bauhaus' ),
        'id'          => 'uni_title',
        'type'        => 'text',
        'desc'        => esc_html__( 'This text will be displayed on the header image', 'bauhaus' ),
      ),
      array(
        'label'       => esc_html__( 'Subtitle on the page', 'bauhaus' ),
        'id'          => 'uni_sub_title',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'You may use html tag <code>br</code> to start a new line in your text.', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Show/Hide "Our story" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_story_enable',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'You can display/hide "Our story" block on this page. Notice: if you fill in with text only one of the columns, only this column will be shown (instead of two).', 'bauhaus' ),
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( '"Our story" title', 'bauhaus' ),
        'id'          => 'uni_our_story_title',
        'type'        => 'text',
        'desc'        => ''
      ),
      array(
        'label'       => esc_html__( 'Text in the left column below "Our story" title', 'bauhaus' ),
        'id'          => 'uni_left_col_text',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'You may use <code>br</code> html tag to start a new line in your text. You can decide to add two or more paragraphs, so just wrap each in <code>p</code> html tag. <code>em, strong, a</code> tags are also allowed.', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Text in the right column below "Our story" title', 'bauhaus' ),
        'id'          => 'uni_right_col_text',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'You may use html tag <code>br</code> to start a new line in your text. You can decide to add two or more paragraphs, so just wrap each in <code>p</code> html tag. <code>em, strong, a</code> tags are also allowed.', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( '"Meet our team"', 'bauhaus' ),
        'id'          => 'tab_team_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Show/Hide "Meet Our Team" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_team_enable',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'off'
      ),
      array(
        'label'       => esc_html__( 'Custom title for "Meet Our Team" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_team_title',
        'type'        => 'text',
        'desc'        => '',
        'condition'   => 'uni_about_team_enable:is(on)'
      ),
      array(
        'id'          => 'uni_meet_team_members',
        'label'       => __( 'Choose users to be displayed on this page', 'bauhaus' ),
        'desc'        => esc_html__( 'This block displays all users with "architector" role by default. You can choose to display only certain users.', 'bauhaus' ),
        'std'         => '',
        'type'        => 'uni-users-type-checkbox',
        'section'     => 'option_types',
        'post_type'   => 'architector',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'label'       => esc_html__( '"Our Partners" section', 'bauhaus' ),
        'id'          => 'tab_values_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Show/Hide "Our Partners" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_brands_enable',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'off'
      ),
      array(
        'label'       => esc_html__( 'Custom title for "Our Partners" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_brands_title',
        'type'        => 'text',
        'desc'        => '',
        'condition'   => 'uni_about_brands_enable:is(on)'
      ),
      array(
        'label'       => esc_html__( 'Instagram block', 'bauhaus' ),
        'id'          => 'tab_instagram_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Show/Hide "Instagram" block on the page', 'bauhaus' ),
        'id'          => 'uni_about_instagram_enable',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'off'
      )
    )
  );

  $contact_meta_box = array(
    'id'          => 'contact_meta_box',
    'title'       => esc_html__( 'Parameters for "Contact" page', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'id'          => 'uni_contact_coordinates',
        'label'       => esc_html__( 'Google map coordinates for this page', 'bauhaus' ),
        'desc'        => esc_html__( 'For example: "41.404182,2.199451". This option overrides global option "Google map coordinates"', 'bauhaus' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_contact_zoom',
        'label'       => esc_html__( 'Google map zoom for this page', 'bauhaus' ),
        'desc'        => esc_html__( 'This option overrides global option "Google map zoom"', 'bauhaus' ),
        'std'         => '14',
        'type'        => 'select',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => '6',
            'label'       => '6',
            'src'         => ''
          ),
          array(
            'value'       => '7',
            'label'       => '7',
            'src'         => ''
          ),
          array(
            'value'       => '8',
            'label'       => '8',
            'src'         => ''
          ),
          array(
            'value'       => '9',
            'label'       => '9',
            'src'         => ''
          ),
          array(
            'value'       => '10',
            'label'       => '10',
            'src'         => ''
          ),
          array(
            'value'       => '11',
            'label'       => '11',
            'src'         => ''
          ),
          array(
            'value'       => '12',
            'label'       => '12',
            'src'         => ''
          ),
          array(
            'value'       => '13',
            'label'       => '13',
            'src'         => ''
          ),
          array(
            'value'       => '14',
            'label'       => '14',
            'src'         => ''
          ),
          array(
            'value'       => '15',
            'label'       => '15',
            'src'         => ''
          ),
          array(
            'value'       => '16',
            'label'       => '16',
            'src'         => ''
          ),
          array(
            'value'       => '17',
            'label'       => '17',
            'src'         => ''
          ),
        )
      ),
      array(
        'id'          => 'uni_marker_colour',
        'label'       => __( 'A colour for marker on this page', 'bauhaus' ),
        'desc'        => __( 'Choose a colour for the map marker on this page. Default is "#000000" (black)', 'bauhaus' ),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_map_styles',
        'label'       => esc_html__( 'Style of the map', 'bauhaus' ),
        'desc'        => '',
        'std'         => 'bauhausGrey',
        'type'        => 'select',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => uni_bauhaus_theme_map_styles()
      ),
      array(
        'id'          => 'uni_contact_address',
        'label'       => esc_html__( 'Address to be displayed on this page', 'bauhaus' ),
        'desc'        => esc_html__( 'This option overrides global option "Address on Contact page". You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_contact_phone',
        'label'       => esc_html__( 'Phone number to be displayed on this page', 'bauhaus' ),
        'desc'        => esc_html__( 'This option overrides global option "Phone number on Contact page". You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_contact_email',
        'label'       => esc_html__( 'Email to be displayed on the "Contact Us" page', 'bauhaus' ),
        'desc'        => esc_html__( 'Also an information submitted via contact form will be sent to this email address. This option overrides global option "Email for Contact page".', 'bauhaus' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_contact_page_wide_enable',
        'label'       => esc_html__( 'Enable/Disable wide contact address field', 'bauhaus' ),
        'desc'        => esc_html__( 'Can be used instead of the three columns.', 'bauhaus' ),
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'home',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_address_page_wide',
        'label'       => esc_html__( 'Address to be displayed on the "Contact Us" page in a wider block, instead of the default three columns', 'bauhaus' ),
        'desc'        => esc_html__('You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus'),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'uni_contact_page_wide_enable:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_contact_page_form_seven_id',
        'label'       => esc_html__( 'CF7 form ID for Contact page', 'bauhaus' ),
        'desc'        => esc_html__( 'Also you can utilise Contact Form 7 form instead of built-in form. Just activate your Contact Form 7 plugin and define here the ID of the form. This option overrides global option "CF7 form ID for Contact page".', 'bauhaus' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'label'       => esc_html__( 'Display/Hide "Map"', 'bauhaus' ),
        'id'          => 'uni_contact_map_enable',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'You can display/hide "Map" on this page.', 'bauhaus' ),
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'Display/Hide block with address', 'bauhaus' ),
        'id'          => 'uni_contact_address_enable',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'You can display/hide block with address.', 'bauhaus' ),
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'Display/Hide contact form', 'bauhaus' ),
        'id'          => 'uni_contact_form_enable',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'You can display/hide contact form', 'bauhaus' ),
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'Title (H3) before contact form', 'bauhaus' ),
        'id'          => 'uni_contact_form_title',
        'type'        => 'textarea',
        'desc'        => esc_html__('You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus'),
        'std'         => esc_html__('Say Hello', 'bauhaus')
      ),
      array(
        'label'       => esc_html__( 'Subtitle before contact form', 'bauhaus' ),
        'id'          => 'uni_contact_form_subtitle',
        'type'        => 'textarea',
        'desc'        => esc_html__('You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus'),
        'std'         => esc_html__('We love to meet people and talk about possibilities', 'bauhaus')
      )
    )
  );

  // for page "Services"
  $servicepage_meta_box = array(
    'id'          => 'servicepage_meta_box',
    'title'       => esc_html__( 'Parameters for "Services" page', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Display/Hide page header with H2 title and image in the bg', 'bauhaus' ),
        'id'          => 'uni_page_header_enable',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'H2 title on the page', 'bauhaus' ),
        'id'          => 'uni_service_title',
        'type'        => 'text',
        'desc'        => esc_html__( 'This text will be displayed on the header image.', 'bauhaus' ),
      ),
      array(
        'label'       => esc_html__( 'Subtitle on the page', 'bauhaus' ),
        'id'          => 'uni_service_sub_title',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'This text will be displayed on the header image. You may use html tags: <code>br, em, strong</code>.', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Display/Hide page title (H1)', 'bauhaus' ),
        'id'          => 'uni_page_title_enable',
        'type'        => 'on-off',
        'desc'        => '',
        'std'         => 'on'
      ),
      array(
        'label'       => esc_html__( 'Custom text under H1', 'bauhaus' ),
        'id'          => 'uni_sub_title_h1',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'This text will be displayed under the page title (H1). You may use html tags: <code>br, em, strong, a</code>.', 'bauhaus' )
      ),
      array(
        'id'          => 'uni_number_of_services',
        'label'       => esc_html__( 'How many prices to display?', 'bauhaus' ),
        'desc'        => esc_html__( 'You can choose between 3 or 4. The chosen number of the newest posts of Prices CPT will be shown.', 'bauhaus' ),
        'std'         => '3',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => '3',
            'label'       => '3',
            'src'         => ''
          ),
          array(
            'value'       => '4',
            'label'       => '4',
            'src'         => ''
          )
        )
      )
    )
  );

  // for cpt "Services"
  $service_meta_box = array(
    'id'          => 'service_meta_box',
    'title'       => esc_html__( 'Parameters for "Service" page', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_service' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'Display this item on the home page', 'bauhaus' ),
        'id'          => 'uni_service_display_home',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'Enable/disable displaying this item on the home page', 'bauhaus' ),
        'std'         => 'off'
      ),
      array(
        'label'       => esc_html__( 'Image for this item for the home page', 'bauhaus' ),
        'id'          => 'uni_service_home_image',
        'type'        => 'gallery',
        'desc'        => esc_html__( 'Add an image for this item to be displayed on the home page. You may add many images, but only the frst one will be used.', 'bauhaus' ),
        'condition'   => 'uni_service_display_home:is(on)'
      ),
      array(
        'label'       => esc_html__( 'Text for this item for the home page', 'bauhaus' ),
        'id'          => 'uni_service_home_text',
        'type'        => 'textarea',
        'desc'        => esc_html__( 'This text will be displayed on home page. It is limited by 30 symbols, so be concise.', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Display this item on "Services" page', 'bauhaus' ),
        'id'          => 'uni_service_display_services',
        'type'        => 'on-off',
        'desc'        => esc_html__( 'Enable/disable displaying this item on "Services" page', 'bauhaus' ),
        'std'         => 'off'
      ),
      array(
        'label'       => esc_html__( 'Image for this item for "Services" page', 'bauhaus' ),
        'id'          => 'uni_service_services_image',
        'type'        => 'gallery',
        'desc'        => esc_html__( 'Add an image for this item to be displayed on "Services" page. You may add many images, but only the frst one will be used.', 'bauhaus' ),
        'condition'   => 'uni_service_display_services:is(on)'
      )
    )
  );

  $slider_meta_box = array(
    'id'          => 'slider_meta_box',
    'title'       => esc_html__( 'Parameters for Home Page Slide', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_home_slides' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'URI for "view all" button', 'bauhaus' ),
        'id'          => 'uni_slide_uri',
        'type'        => 'text',
        'desc'        => esc_html__( 'If you don\'t define an URI for this button, it wo\'nt be shown for this slider', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Label for "view all" button', 'bauhaus' ),
        'id'          => 'uni_slide_label',
        'type'        => 'text',
        'std'         => esc_html__( 'view all', 'bauhaus' ),
        'desc'        => ''
      ),
      array(
        'label'       => esc_html__( 'URI for "view project" button', 'bauhaus' ),
        'id'          => 'uni_slide_uri2',
        'type'        => 'text',
        'desc'        => esc_html__( 'If you don\'t define an URI for this button, it wo\'nt be shown for this slider', 'bauhaus' )
      ),
      array(
        'label'       => esc_html__( 'Label for "view project" button', 'bauhaus' ),
        'id'          => 'uni_slide_label2',
        'type'        => 'text',
        'std'         => esc_html__( 'view project', 'bauhaus' ),
        'desc'        => ''
      ),
      array(
        'id'          => 'uni_slide_text_colour',
        'label'       => esc_html__( 'Colour of texts', 'bauhaus' ),
        'desc'        => esc_html__( 'Choose one of the colours - white or black - as a colour for texts which are over the slide. If your slide is mainly dark then choose "White colour" to make white all the texts. And vise versa. The default colour is "white".', 'bauhaus' ),
        'std'         => 'white',
        'type'        => 'radio',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'white',
            'label'       => esc_html__( 'White colour', 'bauhaus' ),
            'src'         => ''
          ),
          array(
            'value'       => 'black',
            'label'       => esc_html__( 'Black colour', 'bauhaus' ),
            'src'         => ''
          )
        )
      ),
      array(
        'label'       => esc_html__( 'Colours of "view all" button', 'bauhaus' ),
        'id'          => 'tab_colours',
        'type'        => 'tab'
      ),
      array(
        'id'          => 'uni_button_a_colour',
        'label'       => esc_html__( 'Colour for label of "view all" button', 'bauhaus' ),
        'desc'        => '',
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
        'section'     => 'tab_colours',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_button_a_bg',
        'label'       => esc_html__( 'Background colour for "view all" button', 'bauhaus' ),
        'desc'        => '',
        'std'         => '#168cb9',
        'type'        => 'colorpicker',
        'section'     => 'tab_colours',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'uni_button_a_bg_hover',
        'label'       => esc_html__( 'Background colour of hovered state for "view all" button', 'bauhaus' ),
        'desc'        => '',
        'std'         => '#1b9fd2',
        'type'        => 'colorpicker',
        'section'     => 'tab_colours',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );

  $brand_meta_box = array(
    'id'          => 'brand_meta_box',
    'title'       => esc_html__( 'Parameters for "Brand"', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'uni_brand' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional info', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'label'       => esc_html__( 'URI for this brand', 'bauhaus' ),
        'id'          => 'uni_brand_uri',
        'type'        => 'text',
        'desc'        => esc_html__( 'This is an optional parameter. If you don\'t define an URI for this item, then this brand/logo won\'t act as link.', 'bauhaus' )
      )
    )
  );

  $portfoliotwo_meta_box = array(
    'id'          => 'portfoliotwo_meta_box',
    'title'       => esc_html__( 'Parameters for "Portfolio" page template', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional settings', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'id'          => 'uni_portfoliotwo_text_position',
        'label'       => esc_html__( 'Position of content of the page', 'bauhaus' ),
        'desc'        => esc_html__( 'You can choose between top (before portfolio items) and bottom (after portfolio items) positions or leave it disabled if you don\'t want to display the content', 'bauhaus' ),
        'std'         => 'off',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'off',
            'label'       => esc_html__( 'Hide', 'bauhaus' ),
            'src'         => ''
          ),
          array(
            'value'       => 'top',
            'label'       => esc_html__( 'Top', 'bauhaus' ),
            'src'         => ''
          ),
          array(
            'value'       => 'bottom',
            'label'       => esc_html__( 'Bottom', 'bauhaus' ),
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'uni_portfoliotwo_projects',
        'label'       => esc_html__( 'Projects to be shown on this page', 'bauhaus' ),
        'desc'        => esc_html__( 'Because of the specifics of this template, it is better if you choose number of projects that is multiple of 3. For example: 3 or 6 or 9 and so on.', 'bauhaus' ),
        'std'         => '',
        'type'        => 'custom-post-type-checkbox',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => 'uni_project',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );

  $portfoliothree_meta_box = array(
    'id'          => 'portfoliothree_meta_box',
    'title'       => esc_html__( 'Parameters for "Portfolio Three" page template', 'bauhaus' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => esc_html__( 'Additional settings', 'bauhaus' ),
        'id'          => 'tab_add_info',
        'type'        => 'tab'
      ),
      array(
        'id'          => 'uni_portfoliothree_text_position',
        'label'       => esc_html__( 'Position of content of the page', 'bauhaus' ),
        'desc'        => esc_html__( 'You can choose between top (before portfolio items) and bottom (after portfolio items) positions or leave it disabled if you don\'t want to display the content', 'bauhaus' ),
        'std'         => 'off',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'off',
            'label'       => esc_html__( 'Hide', 'bauhaus' ),
            'src'         => ''
          ),
          array(
            'value'       => 'top',
            'label'       => esc_html__( 'Top', 'bauhaus' ),
            'src'         => ''
          ),
          array(
            'value'       => 'bottom',
            'label'       => esc_html__( 'Bottom', 'bauhaus' ),
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'uni_portfoliothree_projects',
        'label'       => esc_html__( 'Projects to be shown on this page', 'bauhaus' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'custom-post-type-checkbox',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => 'uni_project',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );

  /**
   * Register our meta boxes using the
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $project_meta_box );
    ot_register_meta_box( $price_meta_box );
    ot_register_meta_box( $testimonial_meta_box );
    ot_register_meta_box( $about_meta_box );
    ot_register_meta_box( $contact_meta_box );
    ot_register_meta_box( $servicepage_meta_box );
    ot_register_meta_box( $service_meta_box );
    ot_register_meta_box( $slider_meta_box );
    ot_register_meta_box( $brand_meta_box );
    ot_register_meta_box( $portfoliotwo_meta_box );
    ot_register_meta_box( $portfoliothree_meta_box );
}