	<footer class="clear">
			<div class="copyright">
            <?php $aUniAllowedHtmlWithA = uni_bauhaus_theme_allowed_html_with_a(); ?>
            <?php if ( ot_get_option( 'uni_footer_text' ) ) { ?>
                <?php $sFooterText = ot_get_option('uni_footer_text'); echo wp_kses( $sFooterText, $aUniAllowedHtmlWithA ); ?>
            <?php } else { ?>
                <?php echo sprintf( esc_html__('&copy; Copyright %d bauhaus', 'bauhaus' ), date('Y') ); ?>
            <?php } ?>
			</div>
			<div class="footer-social clear">
            <?php if ( ot_get_option( 'uni_email_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_email_link' ) ) ?>" target="_blank"><i class="fa fa-envelope"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_fb_link' ) ) { ?>
				<a href="<?php echo esc_url( ot_get_option( 'uni_fb_link' ) ) ?>" target="_blank"><i class="fa fa-facebook"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_youtube_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_youtube_link' ) ) ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_vimeo_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_vimeo_link' ) ) ?>" target="_blank"><i class="fa fa-vimeo"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_tw_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_tw_link' ) ) ?>" target="_blank"><i class="fa fa-twitter"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_in_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_in_link' ) ) ?>" target="_blank"><i class="fa fa-instagram"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_li_link' ) ) { ?>
        	    <a href="<?php echo esc_url( ot_get_option( 'uni_li_link' ) ) ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_bl_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_bl_link' ) ) ?>" target="_blank"><i class="fa fa-heart"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_pi_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_pi_link' ) ) ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_gp_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_gp_link' ) ) ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_fs_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_fs_link' ) ) ?>" target="_blank"><i class="fa fa-foursquare"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_fl_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_fl_link' ) ) ?>" target="_blank"><i class="fa fa-flickr"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_dr_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_dr_link' ) ) ?>" target="_blank"><i class="fa fa-dribbble"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_be_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_be_link' ) ) ?>" target="_blank"><i class="fa fa-behance"></i></a>
            <?php } ?>
            <?php if ( ot_get_option( 'uni_vk_link' ) ) { ?>
			    <a href="<?php echo esc_url( ot_get_option( 'uni_vk_link' ) ) ?>" target="_blank"><i class="fa fa-vk"></i></a>
            <?php } ?>
			</div>
	</footer>

    <?php wp_nav_menu( array( 'container' => 'div', 'container_class' => 'mobileMenu', 'menu_class' => '', 'theme_location' => 'primary', 'fallback_cb'=> 'uni_bauhaus_theme_nav_mobile_fallback' ) ); ?>

    <?php wp_footer(); ?>
</body>
</html>