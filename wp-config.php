<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'emprende');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(}s7]5!xKpVDdnd`,t-+gLg0u2QO&Skwo65f@8$h^ ),5z-7?#MWHtJb}S(|.hw6');
define('SECURE_AUTH_KEY',  'EJ3t)#TtIC{fJsF#l]&GBvC!}WqQ#8W`d nfKp(siEUYR}|).F[w.t4H*:]p&Tgh');
define('LOGGED_IN_KEY',    'sc/Kfr/s|@iomsvFch^!@994X)P( C75G)k!y+f/qh{v:FFji2Ra5*`w=X&rbNHe');
define('NONCE_KEY',        'ja|45}A kyir-./uRR1>H]Xoce8YjTK}t=Q>3v:|mYDG{LCrd#]_d<o%9-(mtp&W');
define('AUTH_SALT',        '$Q=&i{gb!>vKF^lNKI6:`]E9S7QcL2_<7ebsN%Aj;P%g|ig]jQ,iacnC5c~(O:WB');
define('SECURE_AUTH_SALT', 'CB>iBqH[bc9zx,&Ip=n@S6LgAeLth[e?Q9zgOhDxs([A6f=`FluY6lp(m4pmWFym');
define('LOGGED_IN_SALT',   'v0vP3BQM`WJG:pVbV1g=pW&knm?9 ^CkA6Z_?NMPm#_~E#[T6a~9er,fv+,%}3jS');
define('NONCE_SALT',       'b`}[3}$dxI6077io~j_?sh9miKbr$C[MmZVtLiDsiHt-uAny<[50HNfn+BD$%{Uv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
